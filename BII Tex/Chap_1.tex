\chapter{Oscillazioni e onde}

\section{La corda vibrante}

\subsection{Derivazione dell'equazione}\label{subsec:Derivazione eq d'onda}

\begin{wrapfigure}{o}{0.5\columnwidth}%
\centering{}\includegraphics[bb=0bp 0bp 240bp 148bp,scale=0.8]{corda-grande}
\end{wrapfigure}%
Pizzichiamo una corda in un punto $x$ e chiamiamo $\xi(x,t)$ lo spostamento in direzione $y$ di un elemento della corda centrato in $x$ (detto altrimenti: il grafico sul piano $xy$ di $\xi(x,\overline{t})$ é il profilo della corda all'istante $\overline{t}$). Vogliamo determinare l'equazione che descrive il moto della corda nell'ipotesi che $\xi(x,t)\ll L$, dove L é la lunghezza della corda. Consideriamo un elemento infinitesimo della corda di lunghezza $dl$ e massa $dm=\rho dl$ ($\rho$ è la densitá lineare della corda): l'unica forza agente sull'elemento di corda sará la tensione $T$ della fune. Facciamo il bilancio delle forze tenendo solo i contributi al prim'ordine in $\alpha$ ($\alpha$ è l'angolo formato dalla tangente alla corda in un punto con la direzione orizzontale).

\begin{eqnarray}
F_{x} & = & T(\cos\alpha_{2}-\cos\alpha_{1})\simeq0\\
F_{y} & = & T(\sin\alpha_{2}-\sin\alpha_{1})\simeq\left.\frac{\partial\xi}{\partial x}\right|_{2}-\left.\frac{\partial\xi}{\partial x}\right|_{1}\simeq\frac{\partial^{2}\xi}{\partial x^{2}}dl \label{eq:Fy}
\end{eqnarray}

Notiamo che: 
\begin{itemize}
\item nell'approssimazione che stiamo facendo si ha l'equilibrio lungo la direzione $x$; 
\item nella~\eqref{eq:Fy} abbiamo usato che al prim'ordine $\sin\alpha\simeq\alpha\simeq\tan\alpha={\displaystyle \frac{\partial\xi}{\partial x}}$; 
\item possiamo trascurare l'allungamento della corda: infatti per il teorema di Pitagora il primo termine di allungamento della corda é quadratico e quindi trascurabile. 
\end{itemize}
Andiamo quindi a scrivere l'equazione del moto: 
\[
\rho dl\frac{\partial^{2}\xi}{\partial t^{2}}=T\frac{\partial^{2}\xi}{\partial x^{2}}dl
\]
Da cui, eliminando i $dl$ e ponendo ${\displaystyle \frac{T}{\rho}=c_{s}^{2}}$ (ha le dimensioni di una velocitá al quadrato), si ottiene la celebre equazione di d'Alembert
\begin{equation}
\frac{\partial^{2}\xi}{\partial t^{2}}-c_{s}^{2}\frac{\partial^{2}\xi}{\partial x^{2}}=0
\label{eq:d'Alembert}
\end{equation}



\subsection{Le soluzioni dell'equazione della corda vibrante}

Presentiamo due metodi per cercare soluzioni dell'equazione di d'Alembert. 


\subsubsection*{Cambio di variabile}

Facciamo il seguente cambio di variabile: 
\[
\eta=x-c_{s}t\qquad\qquad\zeta=x+c_{s}t
\]
Si ha quindi per le derivate prime 
\begin{eqnarray*}
\frac{\partial}{\partial x} & = & \frac{\partial}{\partial\eta}+\frac{\partial}{\partial\zeta}\\
\frac{\partial}{\partial t} & = & c_{s}\left(\frac{\partial}{\partial\zeta}-\frac{\partial}{\partial\eta}\right)
\end{eqnarray*}
Mentre per le derivate seconde si trova 
\begin{eqnarray*}
\frac{\partial^{2}}{\partial x^{2}} & = & \frac{\partial^{2}}{\partial\eta^{2}}+\frac{\partial^{2}}{\partial\zeta^{2}}+2\frac{\partial}{\partial\eta}\frac{\partial}{\partial\zeta}\\
\frac{\partial^{2}}{\partial t^{2}} & = & c_{s}^{2}\left(\frac{\partial^{2}}{\partial\eta^{2}}+\frac{\partial^{2}}{\partial\zeta^{2}}-2\frac{\partial}{\partial\eta}\frac{\partial}{\partial\zeta}\right)
\end{eqnarray*}
Sostituendo nella~\eqref{eq:d'Alembert}, l'equazione di d'Alembert assume la forma
\begin{equation}
\frac{\partial^{2}\xi}{\partial\eta\partial\zeta}=0
\end{equation}
che ha come soluzioni 
\[
\xi=\xi_{1}(\eta)+\xi_{2}(\zeta)
\]
Quindi la soluzione nelle variabili $x$ e $t$ é una qualunque funzione del tipo 
\begin{equation}
\xi(x,t)=\xi_{1}(x-c_{s}t)+\xi_{2}(x+c_{s}t)
\label{eq:sol generale}
\end{equation}
dove le due funzioni $\xi_{1}$ e $\xi_{2}$ sono determinate dalle condizioni inziali e al bordo. La~\eqref{eq:sol generale} rappresenta una perturbazione che si propaga a velocitá $c_{s}$. 


\subsubsection*{Separazione delle variabili}

Cerchiamo soluzioni che si possano scrivere nella forma 
\[
\xi(x,t)=A(t)B(x)
\]
L'equazione di d'Alembert diventa, sotto questa ipotesi 
\[
\ddot{A}(t)B(x)=c_{s}^{2}A(t)B''(x)\Longrightarrow\frac{\ddot{A}(t)}{A(t)}=c_{S}^{2}\frac{B''(x)}{B(x)}
\]
Siccome i due membri dell'equazione sopra dipendono da variabili diverse, l'unica possibilitá che hanno per essere uguali é che entrambi siano costanti; si ottengono allora le uguaglianze 
\[
\ddot{A}(t)=-\omega^{2}A(t)\qquad B''(x)=-k^{2}B(x)
\]
dove si é posto ${\displaystyle \frac{\omega}{c_{s}}=k}$. Le soluzioni sono 
\[
A(t)=A_{-}e^{-i\omega t}+A_{+}e^{i\omega t}
\]
\[
B(x)=B_{-}e^{-ikx}+B_{+}e^{ikx}
\]
Quindi si trova come soluzione dell'equazione d'onda 
\begin{equation}
\xi(x,t)=Re[A(t)B(x)]=\xi_{-}\cos[k(x-c_{s}t)]+\xi_{+}\cos[k(x+c_{s}t)]
\end{equation}


Sembrerebbe che questa soluzione sia solo un caso particolare di quella ricavata con l'altro metodo; in realtà vedremo che la linearità dell'equazione rende anche questo tipo di soluzione molto generale.


\subsection{Battimenti}

Mettiamoci in un punto $x$ fissato, ad esempio $x=0$; supponiamo che sulla nostra corda si stia propagando un'onda di frequenza $\omega_{1}$, avremo quindi 
\[
\xi_{1}(0,t)=A_{1}\cos(\omega_{1}t)
\]
Proviamo ad aggiungere un altro termine monocromatico di frequenza $\omega_{2}$:
\[
\xi_{2}(0,t)=A_{2}\cos(\omega_{2}t)
\]
Complessivamente avremo (prendendo per semplicitá il caso $A_{1}=A_{2}=A$)
\[
\xi(0,t)=A(\cos\omega_{1}t+\cos\omega_{2}t)
\]
che, usando le formule di prostaferesi, possiamo riscrivere come 
\[
\xi(t)=2A\cos\left(\frac{\omega_{1}+\omega_{2}}{2}t\right)\cos\left(\frac{\omega_{1}-\omega_{2}}{2}t\right)
\]
Cosa succede se ${\displaystyle \frac{|\omega_{1}-\omega_{2}|}{\omega_{1}}\ll1}$? Se vale questa condizione possiamo porre 
\[
\omega_{1}=\omega+\delta\omega\qquad\omega_{2}=\omega-\delta\omega
\]
ed avremo allora 
\[
\xi(t)=\cos(\omega t)\cos(\delta\omega t)
\]
La figura seguente rappresenta la funzione $\xi(0,t)$ %
\begin{figure}[htb]
\centering
\includegraphics[width=12cm,height=3cm]{Batti_uguali}
\caption{Esempio di battimenti con $A_{1}=A_{2}=A$}
\end{figure}

%
\begin{figure}[htb]
\centering{}\includegraphics[width=12cm,height=3cm]{Batti_dive} 
\caption{Esempio di battimenti con $A_{1}\neq A_{2}$}
\end{figure}


Vediamo che sommando due segnali di frequenza diversa abbiamo ottenuto per sovrapposizione un segnale diverso da entrambi; questa é l'idea alla base della trasformata di Fourier. 


\subsection{Trasformata di Fourier}

Consideriamo una funzione $f(x)\in L^{2}$, cioé tale che esiste finito
\[
\int_{-\infty}^{+\infty}|f(x)|^{2}dx
\]
Si può dimostrare che tale funzione può essere scritta nel modo seguente:
\[
f(x)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\widehat{f}(k)e^{ikx}dk
\]
La funzione $f(x)$ quindi si puó esprimere come una somma di infinite componenti armoniche con frequenze che variano con continuitá tra $-\infty$ e $+\infty$. L'ampiezza di ogni componente é data dalla $\widehat{f}(k)$ che viene chiamata \emph{trasformata di Fourier} della $f$. La trasformata di Fourier si calcola cosí: 
\[
\mathcal{F}(f)\equiv\widehat{f}(k)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}{f}(x)e^{-ikx}dx
\]
La ovvia generalizzazione tridimensionale é 
\[
f(\textbf{x})=\frac{1}{(2\pi)^{3/2}}\int_{-\infty}^{+\infty}\widehat{f}(\textbf{k})e^{i\textbf{k}\cdot\textbf{x}}d^{3}k
\]
Invece se abbiamo a che fare con funzioni della posizione e del tempo, si definisce 
\[
f(x,t)=\frac{1}{2\pi}\int_{-\infty}^{+\infty}\widehat{f}(k,\omega)e^{ikx-i\omega t}dkd\omega
\]
\[
\widehat{f}(k,\omega)=\frac{1}{2\pi}\int_{-\infty}^{+\infty}f(x,t)e^{-ikx+i\omega t}dxdt
\]
Quindi qualunque funzione puó essere scritta come sovrapposizione di onde piane monocromatiche; questo ci permette di affermare che
(almeno per le funzioni di $L^{2}$, che sono poi quelle fisicamente significative) le soluzioni dell'equazione di d'Alembert trovate con i due metodi (cambio di variabile e separazione delle variabili) si equivalgono. 


\subsubsection*{Trasformata della gaussiana\label{trasf-gauss}}

Come esempio calcoliamo la trasformata di Fourier della funzione 
\[
f(x)=Ae^{-x^{2}/L^{2}}
\]
Dobbiamo calcolare il seguente integrale 
\[
\hat{f}(k)=\frac{A}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}e^{-x^{2}/L^{2}-ikx}dx
\]
Ricorriamo al seguente trucco: 
\[
-\frac{x^{2}}{L^{2}}-ikx=-\left(\frac{x^{2}}{L^{2}}+ikx-\frac{k^{2}L^{2}}{4}\right)-\frac{k^{2}L^{2}}{4}=-\left(\frac{x}{L}+\frac{ikL}{2}\right)^{2}-\frac{k^{2}L^{2}}{4}
\]
Quindi la trasformata da calcolare si puó esprimere come 
\[
\hat{f}(k)=\frac{A}{\sqrt{2\pi}}e^{-\frac{k^{2}L^{2}}{4}}\int_{-\infty}^{+\infty}e^{-(x/L+ikL/2)^{2}}dx
\]
che dá il semplice risultato 
\[
\hat{f}(k)=\frac{AL}{\sqrt{2}}e^{-\frac{k^{2}L^{2}}{4}}
\]
Si é trovato quindi che la trasformata di una gaussiana é ancora una gaussiana; osserviamo poi che se la gaussiana é larga in x, allora é stretta in k e viceversa.

%
\begin{figure}[htb]
\centering
\includegraphics[scale=0.9]{fourier_gauss_peg} 
\caption{Una gaussiana con la sua trasformata}
\end{figure}



\subsubsection*{Oscillatore forzato}

Consideriamo l'equazione dell'oscillatore armonico in presenza di un termine forzante dipendente dal tempo: 
\begin{equation}
\ddot{x}+\omega_{0}^{2}x=F(t)
\end{equation}
Applichiamo la trasformata di Fourier a entrambi i membri dell'equazione
\[
\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}F(t)e^{i\omega t}dt=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}(\ddot{x}+\omega_{0}^{2}x)e^{i\omega t}dt
\]
Tramite integrazione per parti\footnote{Stiamo ragionevolmente assumendo che $f(-\infty)=f(+\infty)=0$} si ottiene
\[
\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}(\ddot{x}+\omega_{0}^{2}x)e^{i\omega t}dt=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}(-\omega^{2}+\omega_{0}^{2})xe^{i\omega t}dt=(-\omega^{2}+\omega_{0}^{2})\hat{x}(\omega)
\]
Da cui si ha la relazione
\begin{equation}
\hat{x}(\omega)=\frac{\hat{F}(\omega)}{\omega_{0}^{2}-\omega^{2}}
\label{eq:sol in trasformata}
\end{equation}
Antitrasformando la~\eqref{eq:sol in trasformata} ricaviamo la legge oraria $x(t)$
\[
x(t)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\frac{\hat{F}(\omega)}{\omega_{0}^{2}-\omega^{2}}e^{-i\omega t}d\omega
\]
 


\subsubsection*{Applicazione all'equazione d'onda}

In trasformata di Fourier la derivazione rispetto a $t$ e $x$ risulta particolarmente semplice; si puó dimostrare (semplicemente integrando per parti) che valgono le seguenti regole:
\[
\mathcal{F}(\partial_{t}f)=-i\omega\widehat{f}
\]
\[
\mathcal{F}(\partial_{x}f)=ik\widehat{f}
\]
Applichiamo quindi la trasformata di Fourier ad entrambi i membri dell'equazione di d'Alembert~\eqref{eq:d'Alembert} 
\[
-\omega^{2}\hat{\xi}(\omega,k)+c_{s}^{2}k^{2}\hat{\xi}(\omega,k)=0\qquad\longrightarrow\qquad(\omega^{2}-c_{s}^{2}k^{2})\hat{\xi}(\omega,k)=0
\]
Se vogliamo che la $\xi$ non sia identicamente nulla deve essre verificata la condizione 
\begin{equation}
\omega^{2}=c_{s}^{2}k^{2}
\label{eq:rel di dispersione}
\end{equation}
La~\eqref{eq:rel di dispersione}, che esprime la relazione tra $\omega$ e k, si chiama \textit{relazione di dispersione}; a partire da essa si definiscono la \textit{velocitá di fase} $v_{f}$ e la \textit{velocitá di gruppo} $v_{g}$ 
\[
v_{f}\equiv\frac{\omega}{k}
\]
\[
v_{g}\equiv\frac{\partial\omega}{\partial k}
\]
 


\subsubsection*{Propagazione di un pacchetto d'onda}

Le onde monocromatiche che abbiamo considerato fino ad ora hanno un brutto difetto: si estendono infinitamente nel tempo e nello spazio ed hanno energia infinita! Nella realtà si ha a che fare con segnali costituiti da un numero arbitrario ma finito di oscillazioni: oggetti di questo tipo sono detti
\begin{wrapfigure}{o}{0.4\columnwidth}%
\centering
\includegraphics[bb=0bp 1cm 254bp 157bp,scale=0.75]{gauss}
\end{wrapfigure}%
\textbf{pacchetti d'onda}. Un caso notevole è il pacchetto gaussiano:
\[
f(x)=e^{ik_{0}x-x^{2}/L^{2}}
\]
Il grafico della parte reale di questa funzione é mostrato nella figura accanto: esso é costituito da un certo numero di oscillazioni modulate da un inviluppo gaussiano. Il numero di oscillazioni dipende dal parametro $k_{0}L$:
\begin{eqnarray*}
k_{0}L\gg1 & \rightarrow & molte\,\, oscillazioni\\
k_{0}L\ll1 & \rightarrow & nessuna\,\, oscillazione
\end{eqnarray*}
In particolare per $k_{0}L\rightarrow\infty$ si ha un'onda monocromatica. Vogliamo ora studiare come si propaga un'onda come questa usando la trasformata di Fourier. \\
La nostra condizione iniziale é la seguente: 
\[
\xi(x,t=0)=f(x)
\]
Sappiamo che possiamo scrivere in generale 
\[
\xi(x,t)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\hat{\xi}(\omega,k)e^{ikx-i\omega t}d\omega dk
\]
Sfruttando la relazione di dispersione~\eqref{eq:rel di dispersione}, ci riduciamo a 
\[
\xi(x,t)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\hat{\xi}(k)e^{ik(x-c_{s}t)}dk
\]
Calcolando tutto all'istante $t=0$ otteniamo 
\[
\xi(x,0)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\hat{\xi}(k)e^{ikx}dk=f(x)\Longrightarrow\hat{\xi}(k)=\hat{f}(k)
\]
D'altronde é facile calcolare che 
\[
\hat{f}(k)=\frac{L}{\sqrt{2}}e^{-\frac{1}{4}(k-k_{0})^{2}L^{2}}
\]
Mettendo tutto insieme abbiamo 
\[
\xi(x,t)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\frac{L}{\sqrt{2}}e^{-\frac{1}{4}(k-k_{0})^{2}L^{2}}e^{ik(x-c_{s}t)}dk
\]
Svolgendo l'integrale otteniamo finalmente l'evoluzione temporale del pacchetto:
\begin{equation}
\xi(x,t)=e^{ik_{0}(x-c_{s}t)}e^{-\frac{(x-c_{s}t)^{2}}{L^{2}}}
\label{eq:evoluzione temp}
\end{equation}
Nell'espressione della~\eqref{eq:evoluzione temp} si distingue chiaramente la velocitá di propagazione dell'inviluppo gaussiano dalla velocitá della portante (oscillazioni all'interno); la prima é la velocitá di gruppo, la seconda é la velocitá di fase. Se esse coincidono e sono costanti (come in questo caso) l'onda si dice \emph{non dispersiva}; in generale, nei mezzi dispersivi le due velocitá non coincidono e dipendono dalla frequenza (gli effetti di ció saranno analizzati piú avanti). 


\subsection{Riflessione e onde stazionarie}

Cosa succede se la densitá della corda non é uniforme? Supponiamo di avere due corde caratterizzate da diverse densitá e di legarle
assieme; chiamiamo $\delta$ la lunghezza caratteristica in cui avviene la variazione di densitá; nel problema che stiamo considerando possiamo stimare ${\displaystyle \delta/\lambda}\ll1$ cioé la regione in cui avviene la variazione della densitá é molto piú piccola rispetto alla lunghezza d'onda. Grazie a questa condizione, invece di risolvere l'equazione a coefficienti variabili 
\[
\frac{\partial^{2}\xi}{\partial t^{2}}-c_{s}^{2}(x)\frac{\partial^{2}\xi}{\partial x^{2}}=0
\]
possiamo ricondurci a due equazioni a coefficienti costanti, le cui soluzioni saranno poi raccordate per mezzo di opportune condizioni al contorno. 
\[
\frac{\partial^{2}\xi}{\partial t^{2}}-c_{s,1}^{2}\frac{\partial^{2}\xi}{\partial x^{2}}=0\qquad per\, x<0
\]
\[
\frac{\partial^{2}\xi}{\partial t^{2}}-c_{s,2}^{2}\frac{\partial^{2}\xi}{\partial x^{2}}=0\qquad per\, x>0
\]
Andiamo allora a ricavare le condizioni di raccordo: innanzitutto la $\xi$ deve essere continua, altrimenti la corda si spezzerebbe; inoltre saranno continue anche le sue derivate temporali, dato che la discontinuitá é nello spazio, non nel tempo; naturalmente $\partial_{xx}\xi$ é discontinua perché abbiamo assunto $c_{s}(x)$ discontinua. Ci resta da vedere cosa succede per $\partial_{x}\xi$. Integriamo l'equazione nell'intervallo $[-\varepsilon,\varepsilon]$ (faremo poi tendere $\varepsilon$ a 0) \begin{eqnarray*}
\int_{-\varepsilon}^{\varepsilon}\frac{1}{c_{s}^{2}(x)}\frac{\partial^{2}\xi}{\partial t^{2}}dx & = & \int_{-\varepsilon}^{\varepsilon}\frac{\partial^{2}\xi}{\partial x^{2}}dx\\
2\varepsilon\left\langle \frac{1}{c_{s}^{2}(x)}\frac{\partial^{2}\xi}{\partial t^{2}}\right\rangle  & = & \frac{\partial\xi}{\partial x}(\varepsilon)-\frac{\partial\xi}{\partial x}(-\varepsilon)
\end{eqnarray*}
Mandando $\varepsilon\rightarrow0$ si ottiene 
\begin{equation}
\frac{\partial\xi}{\partial x}(0^{+})-\frac{\partial\xi}{\partial x}(0^{-})=0
\end{equation}
Cioé $\partial_{x}\xi$ é continua in $x=0$. \\
Consideriamo ora un'onda monocromatica e facciamola propagare sulla corda; si avrá 
\begin{eqnarray*}
\xi(x,t) & = & A_{in}e^{ik_{1}x-i\omega_{1}t}\qquad c_{s,1}=\frac{\omega_{1}}{k_{1}}\qquad per\,\, x<0\\
\xi(x,t) & = & A_{tr}e^{ik_{2}x-i\omega_{2}t}\qquad c_{s,2}=\frac{\omega_{2}}{k_{2}}\qquad per\,\, x>0
\end{eqnarray*}
É immediato verificare che con queste soluzioni non si riesce in alcun modo a soddisfare le condizioni di raccordo; per risolvere il
problema basta introdurre un'onda riflessa nella zona $x<0$ 
\begin{eqnarray*}
\xi(x,t) & = & A_{in}e^{ik_{1}x-i\omega_{1}t}+A_{rif}e^{-ik_{3}x-i\omega_{3}t}\qquad\,\,\, per\,\, x<0\\
\xi(x,t) & = & A_{tr}e^{ik_{2}x-i\omega_{2}t}\qquad\qquad\qquad\qquad\qquad per\,\, x>0
\end{eqnarray*}
Il nostro obiettivo é di determinare delle relazioni che leghino le varie $\omega,k\mbox{ e }A$\\
Mettiamoci nel punto $x=0$: la condizione di continuitá della $\xi$ ci fornisce la seguente uguaglianza: 
\[
A_{in}e^{-i\omega_{1}t}+A_{rif}e^{-i\omega_{3}t}=A_{tr}e^{-i\omega_{2}t}
\]
che deve essere soddisfatta $\forall t$; da questo segue necessariamente che $\omega_{1}=\omega_{2}=\omega_{3}$. Ricordando le relazioni che legano le $\omega$ alle $k$ si ricava anche 
\[
k_{1}=k_{3}\qquad k_{2}=\frac{\omega}{c_{s,2}}
\]
Adesso, rimanendo sempre in $x=0$, consideriamo l'istante $t=0$; imponendo la continuitá della $\xi$ e della sua derivata spaziale
troviamo il seguente sistema 
\[
\left\{ \begin{array}{lcr}
A_{in}\,+\, A_{rif} & = & A_{tr}\\
k_{1}(A_{in}-A_{rif}) & = & k_{2}A_{tr}\end{array}\right.
\]
Risolvendo il sistema si ricavano le formule che esprimono $A_{rif}$ e $A_{tr}$ in funzione di$A_{in}$ 
\begin{equation}
A_{rif}=\frac{k_{1}-k_{2}}{k_{1}+k_{2}}A_{in}\qquad\qquad A_{tr}=\frac{2k_{1}}{k_{1}+k_{2}}A_{in}
\label{eq:A_rif e A_tr}
\end{equation}
Cosa succede se, invece di legare tra loro due corde, inchiodo al muro un capo di una corda? Possiamo considerare questa situazione
come un caso particolare della precedente, in cui però si ha che $\rho_{2}\rightarrow\infty$, quindi\footnote{Ricordiamo dal paragrafo~\ref{subsec:Derivazione eq d'onda} che $k=\omega/c_{s}=\omega\rho/T$} anche $k_{2}\rightarrow\infty$; in questo limite le~\eqref{eq:A_rif e A_tr} diventano
\[
A_{rif}=-A_{in}\qquad A_{tr}=0
\]
Al contrario, nel limite $k_{2}\rightarrow0$ si ottiene 
\[
A_{rif}=A_{in}\qquad A_{tr}=2A_{in}
\]



\subsubsection*{Onde stazionarie}

Consideriamo una corda di lunghezza $L$ fissata ai due estremi; rientriamo quindi, secondo la discussione precedente, nel caso $k_{2}\rightarrow\infty$: quando un'onda si propaga sulla corda, una volta raggiunto uno degli estremi, sará totalmente riflessa. Vedremo che per questo problema non tutte le frequenze sono possibili, i modi di oscillazione sono discretizzati. Consideriamo una soluzione del tipo 
\[
\xi(x,t)=A_{1}e^{ikx-i\omega t}+A_{2}e^{-ikx-i\omega t}
\]
Imponiamo che ci sia un nodo (cioé un punto in cui la corda rimane sempre ferma) in $x=0$ e in $x=L$ 
\begin{eqnarray}
\xi(0,t) & = & 0\Rightarrow A_{1}=-A_{2}=A \label{eq:xi(0,t)} \\ 
\xi(L,t) & = & 0\Rightarrow A[e^{ikL}-e^{-ikL}]=0\Rightarrow\sin kL=0 \label{eq:xi(L,t)}
\end{eqnarray}
Dalla~\eqref{eq:xi(0,t)} si ricava che la $\xi$ ha la forma di un'\emph{onda stazionaria}, cioé un'onda in cui non si vede nessuna propagazione:
\[
\xi(x,t)=Ae^{-i\omega t}[e^{ikx}-e^{-ikx}]=2iAe^{-i\omega t}\sin kx
\]
La~\eqref{eq:xi(L,t)} invece determina i valori permessi di $k$: 
\[
kL=n\pi\Rightarrow k=\frac{n\pi}{L}
\]
 


\subsection{Conservazione dell'energia\label{sub:Conservazione-dell'energia-corda}}

Vogliamo scrivere un'equazione che esprima la conservazione dell'energia; ci aspettiamo che essa abbia la struttura di un'\emph{equazione di continuitá}, cioé un'equazione del tipo 
\begin{equation}
\frac{\partial}{\partial t}(\mbox{densitá di energia})+\nabla\cdot(\mbox{un vettore})=(\mbox{densitá di potenza})
\label{eq:conservazione energia}
\end{equation}
Integrando la~\eqref{eq:conservazione energia} in un volume $V$ e ricordando il teorema della divergenza, si trova che la variazione nel tempo dell'energia contenuta in $V$ é pari alla somma dell'energia che fluisce attraverso il contorno di $V$ e della potenza immessa o dissipata in $V$. Visto che stiamo studiando una situazione unidimensionale, cercheremo un'equazione del tipo 
\[
\frac{\partial}{\partial t}(\mbox{densitá lineare di energia})+\frac{\partial}{\partial x}(\mbox{...})=(\mbox{densitá lineare di potenza})
\]
Consideriamo l'equazione della corda vibrante con un termine di sorgente: 
\[
\rho\frac{\partial^{2}\xi}{\partial t^{2}}-T\frac{\partial^{2}\xi}{\partial x^{2}}=S(x,t)
\]
Moltiplichiamo tutto per ${\displaystyle \frac{\partial\xi}{\partial t}}$, ottenendo
\[
\rho\frac{\partial^{2}\xi}{\partial t^{2}}\frac{\partial\xi}{\partial t}-T\frac{\partial^{2}\xi}{\partial x^{2}}{\displaystyle \frac{\partial\xi}{\partial t}}=S(x,t){\displaystyle \frac{\partial\xi}{\partial t}}
\]
che si può riscrivere come 
\[
\frac{\rho}{2}\frac{\partial}{\partial t}\left(\frac{\partial\xi}{\partial t}\right)^{2}-T\frac{\partial}{\partial x}\left(\frac{\partial\xi}{\partial x}\frac{\partial\xi}{\partial t}\right)+\frac{T}{2}\frac{\partial}{\partial t}\left(\frac{\partial\xi}{\partial x}\right)^{2}=S(x,t){\displaystyle \frac{\partial\xi}{\partial t}}
\]
Manipolandola si ricava facilmente l'equazione cercata 
\[
\frac{\partial}{\partial t}\left[\underbrace{\frac{\rho}{2}\,\left(\,\frac{\partial\xi}{\partial t}\,\right)^{2}}_{densit\grave{a}\: di\: energia\: cinetica}+\underbrace{\frac{T}{2}\,\left(\,\frac{\partial\xi}{\partial x}\,\right)^{2}}_{densit\grave{a}\: di\: energia\: potenziale}\right]-T\frac{\partial}{\partial x}\left(\frac{\partial\xi}{\partial x}\frac{\partial\xi}{\partial t}\right)=S(x,t)\frac{\partial\xi}{\partial t}
\]


\section{Onde 3-dimensionali\label{sec:Onde-3-dimensionali}}

La ovvia generalizzazione tridimensionale dell'equazione di d'Alembert è la seguente 
\begin{equation}
\nabla^{2}\xi-\frac{1}{c_{s}^{2}}\frac{\partial^{2}\xi}{\partial t^{2}}=0
\label{eq:tridi}
\end{equation}
Chi legge può verificare che essa ammette soluzioni del tipo \emph{onda piana}, cioè 
\[
\xi(\mathbf{x},t)=A_{+}e^{i\mathbf{k\cdot x}-i\omega t}+A_{-}e^{-i\mathbf{k\cdot x}-i\omega t}
\]
In questo paragrafo mostriamo che la~\eqref{eq:tridi} ammette soluzioni in simmetria sferica. Ricordiamo l'espressione del laplaciano in coordinate sferiche\footnote{Si consiglia di fare il calcolo esplicito, almeno una volta nella vita} (per funzioni che non dipendono da $\theta$ e $\varphi$)
\[
\nabla^{2}=\frac{1}{r^{2}}\frac{\partial}{\partial r}r^{2}\frac{\partial}{\partial r}
\]
In coordinate sferiche la \eqref{eq:tridi} diventa 
\[
\frac{1}{r^{2}}\left(\frac{\partial}{\partial r}r^{2}\frac{\partial}{\partial r}\xi\right)-\frac{1}{c_{s}^{2}}\frac{\partial^{2}\xi}{\partial t^{2}}=0
\]
Per risolvere questa equazione, facciamo il seguente cambio di variabile:
\[
\xi(r,t)=\frac{f(r,t)}{r}
\]
Sostituendo, l'equazione diventa:
\begin{eqnarray*}
\frac{1}{c_{s}^{2}}\frac{\partial^{2}}{\partial t^{2}}\frac{f}{r} & = & \frac{1}{r^{2}}\frac{\partial}{\partial r}\left(r^{2}\frac{\partial}{\partial r}\frac{f}{r}\right)\\
 & = & \frac{1}{r^{2}}\frac{\partial}{\partial r}\left(r\frac{\partial f}{\partial r}-f\right)\\
 & = & \frac{1}{r^{2}}\left(\frac{\partial f}{\partial r}+r\frac{\partial^{2}f}{\partial r^{2}}-\frac{\partial f}{\partial r}\right)\\
 & = & \frac{1}{r}\frac{\partial^{2}f}{\partial r^{2}}
\end{eqnarray*}
Se $r\neq0$ troviamo l'equazione di d'Alembert unidimensionale:
\[
\frac{\partial^{2}f}{\partial r^{2}}-\frac{1}{c_{s}^{2}}\frac{\partial^{2}f}{\partial t^{2}}=0
\]
la cui soluzione generale sappiamo essere
\begin{eqnarray*}
f(r,t) & = & f_{1}(r+c_{s}t)+f_{2}(r-c_{s}t)\\
\Rightarrow\xi(r,t) & = & \frac{f_{1}(r+c_{s}t)}{r}+\frac{f_{2}(r-c_{s}t)}{r}
\end{eqnarray*}


\section{Onde dispersive\label{sec:Onde-dispersive}}

In questa sezione daremo un cenno agli effetti della dispersione; studieremo come esempio le onde di gravità, cioè le onde in cui la
forza di richiamo è la gravità. Onde di questo tipo sono le onde del mare. Questo è il nostro programma: scriveremo la relazione di dispersione e studieremo l'evoluzione di un pacchetto gaussiano, osservando che durante la propagazione esso cambia forma.

Per le onde dispersive si ha che la velocità di fase dipende dalla frequenza, ovvero avremo $v_{f}=v_{f}(\omega)$; quindi ogni frequenza si propaga con velocità differente dalle altre, di conseguenza il pacchetto cambia forma. Come abbiamo detto le onde del mare sono un esempio di onde dispersive e per un mare infinitamente profondo\footnote{Per un mare finito la relazione si modifica nel seguente modo $\omega^{2}=|k|g\tanh(|k|h)$, dove $h$ è la profondità.}, la relazione di dispersione è la seguente:
\[
\omega^{2}=|k|g
\]
dove $g$ è l'accelerazione di gravità, di conseguenza si ha 
\[
v_{f}(\omega)=\sqrt{\left|\frac{g}{k}\right|}=\frac{g}{\omega}\qquad\qquad v_{g}(\omega)=\frac{1}{2}\sqrt{\left|\frac{g}{k}\right|}=\frac{g}{2\omega}
\]
dunque le onde più lunghe viaggiano più veloce. Adesso, vediamo come evolve un pacchetto gaussiano, che prenderemo ben localizzato ovvero $k_{0}L\gg1$, in modo che le frequenze significative sono soltanto quelle concentrate in un intorno di $k_{0}$, così svilupperemo $\omega(k)$ in un intorno di $k_{0}$. Sia il nostro pacchetto
\[
\xi(x,t)=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}\exp\Big[-\frac{1}{4}(k-k_{0})^{2}L^{2}\Big]\exp\big[i(kx-\omega(k)t)\big]dk
\]

Il lettore noti che stiamo lavorando in trasformata poiché si può fare l'evoluzione temporale soltanto frequenza per frequenza. Allora, visto che $k_{0}L\gg1$, possiamo sviluppare il fattore di fase $\exp\big[i(kx-\omega(k)t)\big]$; bisogna fare attenzione però, poiché lo sviluppo sarà valido soltanto per un breve intervallo temporale; facciamo un esempio: considerando il seguente sviluppo
\[
\sin\left((\omega+\delta\omega)t\right)\simeq\sin\omega t+\delta\omega t\cos\omega t\hspace{1em}\text{per}\;\frac{\delta\omega}{\omega}\ll1
\]
si ha che la correzione cresce nel tempo come $\delta\omega t$, quindi il risultato sarà privo di significato per $t\geq1/\delta\omega$. Sviluppiamo $\omega(k)$ in un intorno di $k_{0}$:
\[
\omega(k)=\omega(k_{0})+\left.\frac{\partial\omega}{\partial k}\right|_{k_{0}}(k-k_{0})+\frac{1}{2}\left.\frac{\partial^{2}\omega}{\partial k^{2}}\right|_{k_{0}}(k-k_{0})^{2}+o\left((k-k_{0})^{2}\right)
\]
sostituendo nell'espressione per $\xi(x,t)$, si ottiene 
\begin{multline}
\xi(x,t)\simeq\frac{1}{\sqrt{2\pi}}\exp\big[ik_{0}x-i\omega(k_{0})t\big]\int_{-\infty}^{+\infty}\, dk\,\exp\Big[-\frac{1}{4}(k-k_{0})^{2}L^{2}\Big]\times\\
\exp\big[i(k-k_{0})x\big]\exp\bigg[-i\Big(\frac{\partial\omega}{\partial k}(k-k_{0})+\frac{1}{2}\frac{\partial^{2}\omega}{\partial k^{2}}(k-k_{0})^{2}\Big)t\bigg]
\label{disp-gauss-1}
\end{multline}
\begin{itemize}
\item Trascurando per il momento la correzione al second'ordine si ha il seguente integrale:
\[
\int_{-\infty}^{+\infty}\exp\Big[-\frac{1}{4}(k-k_{0})^{2}L^{2}\Big]\exp\Big[i(k-k_{0})\Big(x-\frac{\partial\omega}{\partial k}t\Big)\Big]d(k-k_{0})
\]
che si risolve facilmente con il ``trucco'' che abbiamo usato per la trasformata della gaussiana a pag.\pageref{trasf-gauss}, infatti si può riscrivere come segue 
\[
\exp\bigg[-\frac{1}{L^{2}}\Big(x-\frac{\partial\omega}{\partial k}t\Big)^{2}\bigg]\int_{-\infty}^{+\infty}\exp\bigg[\Big(\frac{1}{2}(k-k_{0})L-\frac{i}{L}\Big(x-\frac{\partial\omega}{\partial k}t\Big)\Big)^{2}\bigg]d(k-k_{0})
\]
adesso visto che l'integrale è soltanto un fattore costante (è l'integrale di una gaussiana), si ottiene per $\xi(x,t)$ 
\[
\xi(x,t)_{1^\circ ordine}=\frac{\sqrt{2}}{L}\underbrace{\exp\big[ik_{0}x-i\omega(k_{0})t\big]}_{portante}\underbrace{\exp\bigg[-\frac{1}{L^{2}}\Big(x-\frac{\partial\omega}{\partial k}t\Big)^{2}\bigg]}_{inviluppo}
\]
Si noti la differente velocità dell'inviluppo ($v_{g}=\partial_{k}\omega|_{k_{0}}$) rispetto alla portante ($v_{f}=\omega(k_{0})/k_{0}$).
\item Invece se consideriamo anche il secondo ordine abbiamo che:
\[
\xi(x,t)=\exp\big(ik_{0}x-i\omega(k_{0})t\big)\frac{\sqrt{2}}{\sqrt{\left(L^{2}+2i\frac{\partial^{2}\omega}{\partial k^{2}}t\right)}}\exp\left[-\frac{\left(x-\frac{\partial\omega}{\partial k}t\right)^{2}}{\left(L^{2}+2i\frac{\partial^{2}\omega}{\partial k^{2}}t\right)}\right]
\]

Prendendone la parte reale, si ha per l'allargamento del pacchetto la seguente espressione
\[
\mathcal{L}=\frac{1}{L}\sqrt{L^{4}+4\left(\frac{\partial^{2}\omega}{\partial k^{2}}t\right)^{2}}
\]
quindi si vede che il pacchetto si allarga con il tempo. Da notare, però, che il pacchetto mentre si allarga, si abbassa anche, quindi si ha, come è giusto che sia, la conservazione dell'energia\footnote{Questo è vero per i mezzi non dissipativi, per i mezzi dissipativi si deve tenere conto dell'energia assorbita dal mezzo. Vedasi il Teorema di Poynting alla sezione \ref{sec:Teorema-di-Poynting}.}. 

\end{itemize}