\chapter{Onde EM nei mezzi}

\section{Onde elettromagnetiche nei dielettrici}


\subsection{Modello fisico}

Quando si va a studiare la propagazione dei campi elettromagnetici nella materia, ci si può trovare in situazioni molto complicate, in quanto la risposta dei mezzi ai campi è in generale descritta da equazioni \emph{non lineari}. Noi non ci occupiamo di questi casi generali e linearizziamo il problema. Modellizziamo gli elettroni atomici in un mezzo materiale come un sistema di $N$ oscillatori armonici per unità di volume; questa approssimazione lineare è valida solamente nel caso in cui i campi con cui si ha a che fare siano deboli\footnote{Si riveda la discussione del paragrafo~\vref{sec:irraggiamento}}. In queste ipotesi siamo autorizzati a trascurare gli effetti magnetici. Poichè i mezzi sono dispersivi, nello studio della propagazione dei campi, è opportuno analizzare la loro risposta frequenza per frequenza (corrisponde a lavorare in trasformata di Fourier). L'equazione del moto dell'oscillatore forzato è
\begin{equation}
m(\ddot{x}+\gamma\dot{x}+\omega_{0}^{2}x)=-eE_{0}e^{-i\omega t}
\end{equation}
Cerchiamo una soluzione particolare del tipo $x(t)=x_{0}e^{-i\omega t}$; facendo le dovute sostituzioni si ottiene
\begin{equation}
x_{0}=-\frac{eE_{0}}{m(\omega_{0}^{2}-\omega^{2}-i\gamma\omega)}
\end{equation}
Il momento di dipolo indotto dell'atomo è quindi
\begin{equation}
\mathbf{p}=-e\mathbf{x}=\frac{e^{2}}{m(\omega_{0}^{2}-\omega^{2}-i\gamma\omega)}\mathbf{E}
\end{equation}
Possiamo introdurre la \emph{polarizzabilità atomica} $\alpha$ definita come 
\[
\mathbf{p}=\varepsilon_{0}\alpha\mathbf{E}
\]
Il nostro semplice modello allora ci fornisce la seguente espressione per $\alpha$:
\[
\alpha(\omega)=\frac{e^{2}}{\varepsilon_{0}m(\omega_{0}^{2}-\omega^{2}-i\gamma\omega)}
\]
Osserviamo che nel nostro modello abbiamo supposto per semplicità che il mezzo fosse isotropo; in caso contrario avremmo ottenuto per la polarizzabilità un tensore a due indici $\alpha_{ij}$. Adesso calcoliamo la polarizzazione del mezzo, cioè il momento di dipolo per unità di volume; poichè abbiamo $N$ atomi per unità di volume e ognuno di essi ha un momento di dipolo $\mathbf{p}=\varepsilon_{0}\alpha\mathbf{E}$, la polarizzazione è data da 
\[
\mathbf{P}(\omega)=N\mathbf{p}=N\varepsilon_{0}\alpha(\omega)\mathbf{E}=\frac{\omega_{pe}^{2}}{\omega_{0}^{2}-\omega^{2}-i\gamma\omega}\mathbf{E}
\]
dove abbiamo introdotto la cosiddetta \emph{frequenza di plasma}:
\[
\omega_{pe}^{2}=\frac{Ne^{2}}{m\varepsilon_{0}}
\]


\subsection{Equazioni di Maxwell nei dielettrici}

L'esistenza di una polarizzazione della materia implica che ci sono delle cariche e delle correnti di polarizzazione, le quali devono essere inserite come termini di sorgente nelle equazioni di Maxwell.
\begin{eqnarray}
\nabla\cdot\mathbf{E} & = & \frac{\rho_{ext}+\rho_{pol}}{\varepsilon_{0}}\label{eq:div_E_mezzi}\\
c^{2}\nabla\times\mathbf{B} & = & \frac{\mathbf{J}_{ext}+\mathbf{J}_{pol}}{\varepsilon_{0}}+\frac{\partial\mathbf{E}}{\partial t}
\label{eq:rot_B_mezzi}
\end{eqnarray}
Sappiamo che la densità di carica di polarizzazione si può scrivere come
\begin{equation}
\rho_{pol}=-\nabla\cdot\mathbf{P}
\end{equation}
Per calcolare la corrente di polarizzazione usiamo l'equazione di continuità:
\[
\nabla\cdot\mathbf{J}_{pol}=-\frac{\partial\rho_{pol}}{\partial t}=\frac{\partial}{\partial t}\nabla\cdot\mathbf{P}=\nabla\cdot\frac{\partial\mathbf{P}}{\partial t}
\]
\begin{equation}
\Rightarrow\mathbf{J}_{pol}=\frac{\partial\mathbf{P}}{\partial t}
\label{eq:J_pol}
\end{equation}
Sostituendo le relazioni appena trovate\footnote{In realtà la \eqref{eq:J_pol} è vera a meno di un rotore, cioè a meno della corrente di magnetizzazione $\mathbf{J}_{mag}=\nabla\times\mathbf{M}$, che però è esclusa dalla nostra trattazione.} nelle \eqref{eq:div_E_mezzi} e \eqref{eq:rot_B_mezzi}, si ottiene
\begin{eqnarray*}
\nabla\cdot\mathbf{E} & = & \frac{\rho_{ext}-\nabla\cdot\mathbf{P}}{\varepsilon_{0}}\\
\varepsilon_{0}c^{2}\nabla\times\mathbf{B} & = & \mathbf{J}_{ext}+\frac{\partial}{\partial t}(\varepsilon_{0}\mathbf{E}+\mathbf{P})
\end{eqnarray*}
mentre le equazioni di Maxwell omogenee rimangono immutate. Osserviamo che, per scrivere queste equazioni in modo più compatto, si potrebbe introdurre, come è stato fatto in elettrostatica, il vettore spostamento elettrico $\mathbf{D}\equiv\varepsilon_{0}\mathbf{E}+\mathbf{P}$; tuttavia nel caso di campi variabili il vettore \textbf{D} non è molto
comodo. Infatti mentre si ha
\[
\mathbf{D}(\omega)\propto\mathbf{E}(\omega)
\]
la relazione tra $\mathbf{E}$ e $\mathbf{D}$ non è istantanea\footnote{Per ricavare $\mathbf{D}(t)$ da $\mathbf{E}(t)$ bisogna esprimere $\mathbf{D}(\omega)$ in funzione di $\mathbf{E}(\omega)$ e poi eseguire un'antitrasformata di Fourier: $\mathbf{D}(t)=\frac{1}{\sqrt{2\pi}}\int\varepsilon(\omega)\mathbf{E}(\omega)e^{-i\omega t}d\omega$}
\[
\mathbf{D}(t)\neq\mathbf{E}(t)
\]
In seguito quindi non faremo mai uso del vettore $\mathbf{D}$, ma scriveremo tutto in termini dei campi $\mathbf{E}$ e $\mathbf{B}$.

Il nostro prossimo obiettivo è trovare l'equazione d'onda nei dielettrici; mettiamoci nel caso in cui $\rho_{ext}=0$ e $\mathbf{J}_{ext}=0$.
Procediamo come nel vuoto, cioè applichiamo il rotore all'equazione di Faraday
\begin{eqnarray*}
\nabla\times(\nabla\times\mathbf{E}) & = & -\frac{\partial}{\partial t}(\nabla\times\mathbf{B})\\
-\nabla^{2}\mathbf{E}+\nabla(\nabla\cdot\mathbf{E}) & = & -\frac{1}{c^{2}}\frac{\partial}{\partial t}\left[\frac{\partial}{\partial t}\left(\frac{\mathbf{P}}{\varepsilon_{0}}+\mathbf{E}\right)\right]\\
-\nabla^{2}\mathbf{E}+\nabla(\nabla\cdot\mathbf{E}) & = & -\frac{1}{c^{2}}\frac{\partial^{2}}{\partial t^{2}}\left(\frac{\mathbf{P}}{\varepsilon_{0}}+\mathbf{E}\right)
\end{eqnarray*}
Quanto vale $\nabla\cdot\mathbf{E}$?
\[
\nabla\cdot\mathbf{E}=-\frac{\nabla\cdot\mathbf{P}}{\varepsilon_{0}}=-N\alpha(\omega)\nabla\cdot\mathbf{E}
\]
\[
\Rightarrow\nabla\cdot\mathbf{E}(1+N\alpha)=0
\]
Possiamo distinguere allora i due seguenti casi:
\begin{eqnarray*}
\nabla\cdot\mathbf{E}=0 & \rightarrow & \emph{\text{onde trasversali}} \\ 
1+N\alpha=0 & \rightarrow & \emph{\text{onde longitudinali}}
\end{eqnarray*}
E' interessante notare che, diversamente dal caso nel vuoto, nella materia sono possibili tre stati di polarizzazione: si possono avere anche onde a polarizzazione longitudinale; si può mostrare che per queste onde il campo magnetico è nullo, ed è nulla anche la velocità di gruppo\footnote{Si provi a risolvere gli esercizi 13.5 e 15.7}. Nel seguito ci occuperemo solamente delle onde trasversali, per cui assumiamo $\nabla\cdot\mathbf{E}=0$; l'equazione che descrive la propagazione del campo elettrico nel mezzo è quindi
\begin{equation}
\nabla^{2}\mathbf{E}-\frac{1}{c^{2}}\frac{\partial^{2}\mathbf{E}}{\partial t^{2}}=\frac{1}{\varepsilon_{0}c^{2}}\frac{\partial^{2}\mathbf{P}}{\partial t^{2}}
\label{eq:onde_con_pol}
\end{equation}
Con procedimento analogo si può ottenere l'equazione d'onda per il campo magnetico:
\begin{equation}
\nabla^{2}\mathbf{B}-\frac{1}{c^{2}}\frac{\partial^{2}\mathbf{B}}{\partial t^{2}}=-\frac{1}{\varepsilon_{0}c^2}\nabla\times\frac{\partial\mathbf{P}}{\partial t}
\label{eq:onde_con_pol_B}
\end{equation}
Adesso cerchiamo soluzioni di queste equazioni del tipo onda piana monocromatica
\[
\mathbf{E}=\mathbf{E}_{0}e^{ikx-i\omega t}
\]
Sostituendo questa soluzione nella \eqref{eq:onde_con_pol} si ottiene la relazione di dispersione:
\begin{equation}
k^{2}c^{2}=\omega^{2}\left[1+N\alpha(\omega)\right]
\end{equation}
Introduciamo l'\emph{indice di rifrazione ${\displaystyle n\equiv\frac{c}{v_{f}}=\frac{kc}{\omega}}$}. Dalla relazione di dispersione scritta sopra si deduce che $n(\omega)=\sqrt{1+N\alpha(\omega)}=\sqrt{\varepsilon_{r}(\omega)}$.  

Osserviamo che il numero d'onda $k$ (e quindi anche l'indice di rifrazione) è composto in generale da una parte reale e una parte immaginaria, $k=k_{r}+ik_{i}$. Questo comporta che l'ampiezza dell'onda si attenua durante la propagazione; infatti inserendo il $k$ nell'espressione dell'onda piana si trova
\[
\mathbf{E}=\mathbf{E}_{0}e^{-k_{i}x}e^{ik_{r}x-i\omega t}
\]


\subsubsection*{Limite del continuo}

Vogliamo vedere se la risposta dielettrica dipende dal fatto che la materia sia discreta; introduciamo un parametro $\lambda$ e trasformiamo le grandezze in gioco nel modo seguente:
\begin{eqnarray*}
N & \rightarrow & \lambda N\\
m & \rightarrow & m/\lambda\\
e & \rightarrow & e/\lambda
\end{eqnarray*}
Considerare il limite $\lambda\rightarrow\infty$ corrisponde a vedere il mezzo non più discreto, ma come una distribuzione continua di materia; effettuando le sostituzioni nell'espressione di \textbf{P} si scopre che essa rimane invariata, cioè la risposta dielettrica non ``si accorge'' del fatto che il mezzo sia discreto.

\section{Onde nei conduttori}

Nei conduttori (metalli, plasmi), l'equazione che descrive il moto degli elettroni sotto l'azione di un campo elettrico oscillante può essere scritta nel seguente modo 
\begin{equation}
m(\ddot{x}+\gamma\dot{x})=-eE_{0}e^{-i\omega t}
\end{equation}
Questa equazione è identica a quella che abbiamo risolto per trovare la risposta dielettrica, a parte il fatto che qui si ha $\omega_{0}=0$, dato che in un conduttore gli elettroni sono liberi. La soluzione dell'equazione è
\[
\mathbf{v}(t)=-\frac{e}{m(-i\omega+\gamma)}\mathbf{E}
\]
\[
\Rightarrow\mathbf{J}=-Ne\mathbf{v}=\frac{Ne^{2}}{m(-i\omega+\gamma)}\mathbf{E=}\frac{\omega_{pe}^{2}}{(-i\omega+\gamma)}\varepsilon_{0}\mathbf{E}
\]
Possiamo introdurre la conducibilità generalizzata $\sigma$: essa sarà in generale una quantità complessa. Studiamo il suo comportamento limite in due casi particolari:
\begin{eqnarray*}
\omega\ll\gamma & \rightarrow & \sigma\simeq\frac{Ne^{2}}{m\gamma}\qquad \emph{\text{corrente in fase con il campo}}\\
\omega\gg\gamma & \rightarrow & \sigma\simeq i\frac{Ne^{2}}{m\omega}\qquad \emph{\text{corrente sfasata di $\pi/2$ con il campo}}
\end{eqnarray*}
Il primo caso corrisponde al limite ohmico in cui la conducibilità \emph{non} dipende dalla frequenza; nel secondo caso invece si ha
a che fare con frequenze elevate e si verifica che la potenza dissipata è nulla, in quanto $\left\langle \mathbf{J\cdot E}\right\rangle =0$.

Studiamo ora la propagazione di onde elettromagnetiche in un conduttore. L'equazione che descrive la propagazione del campo elettrico è 
\[
\nabla^{2}\mathbf{E}-\frac{1}{c^{2}}\frac{\partial^{2}\mathbf{E}}{\partial t^{2}}=\frac{1}{\varepsilon_{0}c^{2}}\frac{\partial\mathbf{J}}{\partial t}
\]
Se sostituiamo l'espressione per \textbf{J} trovata sopra e imponiamo una soluzione del tipo $\mathbf{E}=\mathbf{E}_{0}e^{ikx-i\omega t}$, si ottiene la relazione di dispersione
\[
\omega^{2}=k^{2}c^{2}+\frac{i\omega}{(i\omega-\gamma)}\omega_{pe}^{2}
\]
Studiamo la risposta del conduttore a basse ed alte frequenze:
\begin{itemize}
\item $\omega\ll\gamma$: la relazione di dispersione si può approssimare nel modo seguente:
\[
\omega^{2}=k^{2}c^{2}+\frac{\omega}{i\gamma}\omega_{pe}^{2}
\]
da cui si ricava $k$ 
\[
k^{2}\simeq\frac{i\omega}{\gamma c^{2}}\omega_{pe}^{2}\quad\Rightarrow\quad k=\frac{1+i}{\sqrt{2}c}\sqrt{\frac{\omega\omega_{pe}^{2}}{\gamma}}
\]
L'onda all'interno del conduttore ha allora la forma
\[
\mathbf{E}=\mathbf{E}_{0}e^{-x/l_{s}}\left(e^{ix/l_{s}-i\omega t}\right)
\]
cioè l'onda si smorza su una lunghezza 
\[
l_{s}=\frac{c}{\omega_{pe}}\sqrt{\frac{2\gamma}{\omega}}
\]
detta \emph{lunghezza di pelle collisionale}.
\item $\gamma\ll\omega$: la relazione di dispersione diventa
\[
\omega^{2}=k^{2}c^{2}+\omega_{pe}^{2}
\]
quindi 
\[
k^{2}=\frac{\omega^{2}-\omega_{pe}^{2}}{c^{2}}
\]
da qua si deduce che se $\omega<\omega_{pe}$ il numero d'onda $k$ è immaginario puro e dunque il campo elettrico all'interno del conduttore è del tipo
\[
\mathbf{E}=\mathbf{E}_{0}e^{-x/l_{p}}e^{-i\omega t}
\]
non si ha quindi propagazione all'interno del mezzo. I campi penetrano nel mezzo solamente fino ad una distanza
\[
l_{p}=\sqrt{\frac{c^{2}}{\omega_{pe}^{2}-\omega^{2}}}
\]
detta \emph{lunghezza di pelle inerziale}. Onde di questo tipo sono dette \emph{evanescenti}.
\end{itemize}

\subsection{Energia trasportata da un'onda evanescente}

Calcoliamo il flusso d'energia trasportato da un'onda evanescente. I campi sono dati da
\begin{eqnarray*}
\mathbf{E} & = & E_{0}\hat{\mathbf{y}}e^{-x/l_{p}}\cos\omega t\\
\mathbf{B} & = & \frac{E_{0}}{\omega l_{p}}\hat{\mathbf{z}}e^{-x/l_{p}}\sin\omega t
\end{eqnarray*}
Il vettore di Poynting è
\[
\mathbf{S}=\varepsilon_{0}c^{2}\mathbf{E}\times\mathbf{B}=\varepsilon_{0}c^{2}\frac{E_{0}^{2}}{\omega l_{p}}e^{-2x/l_{p}}\sin(2\omega t)
\]
Mediando sul periodo si ottiene che il trasporto netto di energia per un'onda evanescente è nullo:
\[
\left\langle \mathbf{S}\right\rangle =0
\]



\section{Onde in mezzi disomogenei}

Consideriamo ora il problema della propagazione di onde elettromagnetiche in mezzi disomogenei. In generale si dovrebbero risolvere equazioni molto complicate, poichè la risposta del mezzo ai campi è una funzione della posizione ($\alpha=\alpha(\omega,\mathbf{x})$) dunque la soluzione del tipo $\mathbf{E}_{0}e^{ikx-i\omega t}$ non funziona più. Esistono però due situazioni in cui si possono fare delle approssimazioni che rendono il problema notevolmente più semplice; sia $\lambda$ la lunghezza d'onda della radiazione che si sta propagando in un mezzo le cui proprietà ottiche variano su una scala di lunghezza $L$ (ad esempio $L$ può essere la lunghezza sulla quale in media l'indice di rifrazione si dimezza). I casi interessanti sono i seguenti:  
\begin{itemize}
\item $\lambda/L\gg1$: la variazione delle proprietà del mezzo avvengono in una zona così piccola che possiamo approssimare la variazione con una discontinuità (approsimazione impulsiva) 
\item $\lambda/L\ll1$: le proprietà del mezzo variano molto lentamente rispetto alla lunghezza d'onda (approssimazione adiabatica) $\rightarrow$ ottica geometrica
\end{itemize}

\subsection{Riflessione e rifrazione}

Poniamoci nelle ipotesi dell'approssimazione impulsiva; possiamo farci un'idea della situazione dalla figura \ref{fig:Approssimazione-impulsiva}: il grafico a sinistra mostra l'andamento dell'indice di rifrazione di un mezzo in funzione di $x/\lambda$ (posizione espressa in unità di lunghezza d'onda); a destra, nelle stesse unità, è disegnata una funzione discontinua a gradino che approssima l'indice di rifrazione.
\begin{figure}
\begin{center}
\subfloat{
\centering
\includegraphics[scale=0.7]{indice-rifrazione}
}~~~~~~~~~~~%
\subfloat{
\centering
\includegraphics[scale=0.7]{indice-rifrazione2}
}
\end{center}
\caption{Approssimazione impulsiva\label{fig:Approssimazione-impulsiva}}
\end{figure}
Consideriamo allora due mezzi omogenei aventi indici di rifrazione reali (o che la parte immaginaria sia trascurabile rispetto a quella reale) $n_{1}$ e $n_{2}$ separati da un'interfaccia. Mandiamo un'onda piana monocromatica sulla superficie di separazione $x=0$: in generale nella zona $x<0$ si avrà la sovrapposizione dell'onda incidente con un'onda riflessa, invece, per $x>0$ ci sarà l'onda trasmessa. Per determinare le caratteristiche dei campi in tutto lo spazio abbiamo bisogno di conoscere le condizioni di raccordo su una discontinuità.


\subsubsection{Condizioni di raccordo dei campi}

Elenchiamo subito le condizioni che ci serviranno:
\begin{itemize}
\item la componente del campo elettrico tangente alla superficie di separazione è continua:
\begin{equation}
E_{tan}(0^{-})=E_{tan}(0^{+})
\label{eq:E tan}
\end{equation}
\item tutte le componenti del campo magnetico $\mathbf{B}$ sono continue:
\begin{equation}
\mathbf{B}(0^{-})=\mathbf{B}(0^{+})
\label{eq:B continuo}
\end{equation}
\end{itemize}
Anche se non ci servirà, facciamo notare che la componente ortogonale di $\mathbf{D}\equiv\varepsilon_{0}\mathbf{E}+\mathbf{P}$ è continua.

Dimostriamo la \eqref{eq:E tan}: consideriamo l'equazione di Faraday $\nabla\times\mathbf{E}=-\partial_{t}\mathbf{B}$ e applichiamo il teorema di Stokes su una spiretta rettangolare di lati $l$ e $\delta$ ``a cavallo'' della superficie di separazione
\[
\left[E_{tan}(-\delta)-E_{tan}(\delta)\right]l=-\frac{d}{dt}\int\mathbf{B\cdot}d\mathbf{s}
\]
nel limite $\delta\rightarrow0$ si ottiene, se \textbf{B} si mantiene finito (e non c'è motivo per cui debba essere diversamente), $E_{tan}(0^{-})=E_{tan}(0^{+})$.

Per dimostrare la \eqref{eq:B continuo} occorre utilizzare le equazioni di Maxwell relative al campo magnetico e seguire un ragionamento simile a quanto fatto sopra. Osserviamo che se sulla superficie di separazione vi è una corrente superficiale (è il caso dello specchio perfetto) in generale la componente tangenziale del campo magnetico \emph{non} è continua.

All'interno del nostro semplice modello possiamo però considerare l'intero campo magnetico come continuo. Per la conservazione del campo magnetio perpendicolare si procede integrando un piccolo volume gaussiano sul bordo della superficie. 

\begin{equation}
	(B_{par}(-h/2) - B_{par}(h/2)) l = \oint \mathbf{B} d\mathbf{l} = \int \curl{\mathbf{B}} d\mathbf{S}
\end{equation}

\subsubsection{Le leggi della riflessione e della rifrazione}



Facciamo incidere un'onda piana monocromatica polarizzata linearmente sulla superficie di separazione tra due mezzi omogenei e isotropi. Chiamiamo $\theta_{i},\:\theta_{r},\:\theta_{t}$ l'angolo formato dalla normale alla 
\begin{wrapfigure}{o}{0.4\columnwidth}%
\centering
\includegraphics[bb=60bp 16bp 200bp 240bp, scale=0.6]{rifrazionesl}
\par
\caption{Riflessione e rifrazione\label{fig:Riflessione-e-rifrazione}}
\end{wrapfigure}% 
superficie con la direzione di propagazione rispettivamente dell'onda incidente, riflessa e trasmessa (si veda la figura \ref{fig:Riflessione-e-rifrazione}). Definiamo \emph{piano d'incidenza} il piano generato dalla normale alla superficie e dal vettore d'onda dell'onda incidente. Diremo che un'onda ha polarizzazione P se la direzione del campo elettrico è parallela al piano d'incidenza, mentre chiameremo polarizzazione S quella di un'onda il cui campo elettrico è diretto perpendicolarmente al piano d'incidenza. 


Il campo elettrico in tutto lo spazio è dato da
\begin{eqnarray*}
\mathbf{E} & = & \mathbf{E}_{i}e^{i\mathbf{k\cdot x}-i\omega t}+\mathbf{E}_{r}e^{i\mathbf{k'\cdot x}-i\omega't}\qquad\qquad x<0\\
\mathbf{E} & = & \mathbf{E}_{t}e^{i\mathbf{k"\cdot x}-i\omega"t}\,\,\qquad\qquad\qquad\qquad\quad\, x>0
\end{eqnarray*}
dalla continuità della componente tangenziale del campo elettrico in $\mathbf{x}=(0,0,0)$ si ricava
\begin{equation}
E_{i\parallel}e^{-i\omega t}+E_{r\parallel}e^{-i\omega't}=E_{t\parallel}e^{-i\omega"t}
\end{equation}
questa uguaglianza deve essere verificata $\forall t$, perciò dev'essere necessariamente $\omega'=\omega"=\omega$. 

Se invece poniamo $t=0$ e $x=0$ si ha
\[
E_{i\parallel}e^{ik_{y}y}+E_{r\parallel}e^{ik'_{y}y}=E_{t\parallel}e^{ik"_{y}y}
\]
da cui segue $k'_{y}=k"_{y}=k_{y}$. Un'altra uguaglianza ci è fornita dalla definizione di indice di rifrazione, infatti 
\[
n_{1}=\frac{\left|\mathbf{k}\right|c}{\omega}=\frac{\left|\mathbf{k'}\right|c}{\omega}\:\Rightarrow\:\left|\mathbf{k}\right|=\left|\mathbf{k'}\right|
\]
Dato che le componenti dei vettori d'onda lungo $y$ sono uguali si ricava che dev'essere\footnote{Non consideriamo la soluzione $k'_{x}=k_{x}$ perchè stiamo cercando un'onda riflessa, non un'altra onda incidente.} $k'_{x}=-k_{x}$. La relazione tra gli angoli di incidenza e di riflessione è data quindi dalla \emph{legge di Cartesio}
\[
\theta_{r}=\theta_{i}
\]

Occupiamoci ora della direzione dell'onda trasmessa; dalla definizione di indice di rifrazione si ha il sistema
\[
\begin{cases}
n_{1}= & \frac{c\sqrt{k_{x}^{2}+k_{y}^{2}}}{\omega}\\
n_{2}= & \frac{c\sqrt{k"{}_{x}^{2}+k_{y}^{2}}}{\omega}
\end{cases}
\]
risolvendo questo sistema si ottiene (non riportiamo i dettagli dei calcoli)
\[
\frac{n_{2}^{2}}{n_{1}^{2}}=\frac{\left(\frac{k_{x}"}{k_{y}}\right)^{2}+1}{\left(\frac{k_{x}}{k_{y}}\right)^{2}+1}=\frac{\sin^{2}\theta_{i}}{\sin^{2}\theta_{t}}
\]
troviamo quindi la \emph{legge di Snell} per la rifrazione
\[
n_{1}\sin\theta_{i}=n_{2}\sin\theta_{t}
\]
Facciamo un'osservazione interessante: se $n_{1}>n_{2}$ esiste un'angolo limite $\theta_{l}=\arcsin\frac{n_{2}}{n_{1}}$ per il quale $\theta_{t}=\pi/2$; per angoli di incidenza superiori a $\theta_{l}$ non si ha trasmissione (fenomeno della riflessione totale). Se analizziamo il fenomeno con più attenzione, vediamo che per angoli superiori all'angolo limite $\mathbf{k"}$ diventa immaginario, cioè l'onda trasmessa è evanescente:
\begin{eqnarray*}
k"{}_{x}^{2}+k_{y}^{2} & = & \frac{\omega^{2}}{c^{2}}n_{2}^{2}\\
k"{}_{x}^{2}+k^{2}\sin^{2}\theta_{i} & = & \frac{\omega^{2}}{c^{2}}n_{2}^{2}\\
\Rightarrow k"{}_{x}^{2} & = & \frac{\omega^{2}}{c^{2}}(n_{2}^{2}-n_{1}^{2}\sin^{2}\theta_{i})
\end{eqnarray*}
Osserviamo che per quanto è stato ricavato sulla rifrazione è essenziale l'ipotesi di isotropia del mezzo; in caso contrario avremmo dovuto tenere conto della polarizzazione dell'onda incidente poichè in generale a polarizzazioni diverse corrispondono indici di rifrazione diversi\footnote{Si provi a risolvere il problema 2 del compito del 09/09/09} (fenomeno della \emph{birifrangenza}).


\subsubsection*{Effetto tunnel elettromagnetico}

\begin{wrapfigure}{O}{0.55\columnwidth}%
\subfloat[]{
\centering
\includegraphics{tunnel11}
} 
\subfloat[]{
\centering
\includegraphics[scale=0.25]{tunnel22}
}
\end{wrapfigure}%
Abbiamo visto che le onde evanescenti non trasportano energia, dunque quando della radiazione a frequenza $\omega<\omega_{pe}$ viene fatta incidere su un foglio di metallo, ci aspetteremmo che non si possa rilevare una radiazione trasmessa dall'altra parte del foglio. Invece si osserva che se il foglio è abbastanza sottile (minore della lunghezza di pelle), si ha un'onda trasmessa. Vediamo perchè accade ciò. Guardiamo la figura (a): l'onda incide sul foglio di metallo e all'interno si propaga un'onda evanescente; questa poi si riflette sulla superficie destra del foglio; per sapere come è fatta quest'onda riflessa bisogna imporre le condizioni di raccordo dei campi che abbiamo ricavato poco prima: si scopre che l'onda riflessa è sfasata rispetto a quella incidente (sulla superficie destra del foglio). Allora all'interno del metallo abbiamo la sovrapposizione di due onde evanescenti sfasate tra di
loro: è proprio questo sfasamento che fa sì che la media del vettore di Poynting non sia più nulla all'interno del conduttore. Il fenomeno descritto viene chiamato \emph{effetto tunnel elettromagnetico} ed ha luogo ogni volta che si ha una zona di evanescenza frapposta a due zone di propagazione. Un altro esempio è mostrato in figura (b): della radiazione (tipicamente microonde) incide su di un prisma in modo tale che, quando la luce raggiunge il lato obliquo del prisma, vi incida con un angolo superiore all'angolo limite e si abbia perciò riflessione totale. Se davanti al lato obliquo mettiamo un altro prisma, si osserva che la radiazione penetra all'interno di quest'ultimo; infatti anche in questo caso si ha una zona di evanescenza interposta a due zone di propagazione.  

\subsubsection{Formule di Fresnel}

Il nostro prossimo obiettivo è la determinazione delle ampiezze delle onde riflesse e trasmesse. Dobbiamo distinguere tra polarizzazione S e P. Mostriamo il calcolo per la polarizzazione S. Il campo magnetico è dato da
\[
B_{y}=-\frac{k_{x}E_{i}}{\omega}\qquad\qquad B'_{y}=-\frac{k'_{x}E_{r}}{\omega}\qquad\qquad B"_{y}=-\frac{k"_{x}E_{t}}{\omega}
\]
La condizione di continuità del campo magnetico impone
\[
B_{y}+B'_{y}=B"_{y}
\]
accoppiando questa uguaglianza con la condizione di continuità di \textbf{E} si ottiene un sistema che conduce alle \emph{Formule di
Fresnel} 
\[
\begin{cases}
E_{r}= & \frac{k_{x}-k"_{x}}{k_{x}+k"_{x}}E_{i}\\
E_{t}= & \frac{2k_{x}}{k_{x}+k"_{x}}E_{i}
\end{cases}
\]
Per la polarizzazione P i calcoli sono simili e si arriva a
\begin{equation}
\begin{cases}
E_{r}= & \frac{n_{2}^{2}k_{x}-n_{1}^{2}k"_{x}}{n_{2}^{2}k_{x}+n_{1}^{2}k"_{x}}E_{i}\\
E_{t}= & \frac{2n_{1}n_{2}k_{x}}{n_{2}^{2}k_{x}+n_{1}^{2}k"_{x}}E_{i}
\end{cases}
\label{eq:Fresnel P}
\end{equation}


\subsubsection{Angolo di Brewster}

Facciamo vedere adesso che per onde polarizzate P esiste un angolo di incidenza (angolo di \emph{Brewster}) per il quale non si ha onda riflessa. Basta porre nelle \eqref{eq:Fresnel P} $E_{r}=0$:
\begin{eqnarray*}
n_{2}^{2}k_{x} & = & n_{1}^{2}k"_{x}\\
n_{2}^{2}\frac{\omega n_{1}}{c}\cos\theta_{i} & = & n_{1}^{2}\frac{\omega n_{2}}{c}\cos\theta_{t}\\
\sin2\theta_{i} & = & \sin2\theta_{t}\\
\Rightarrow\theta_{i}+\theta_{t} & = & \frac{\pi}{2}
\end{eqnarray*}
In generale se abbiamo un'onda con polarizzazione lineare in una direzione generica, possiamo, grazie al principio di sovrapposizione, scriverla come somma di due onde, una polarizzata P e una S; se quest'onda incide all'angolo di Brewster, l'onda riflessa avrà polarizzazione puramente S.
\begin{wrapfigure}{o}{0.4\columnwidth}%
\centering
\includegraphics[scale=0.55]{brewster2}
\end{wrapfigure}%
Vediamo ora come si può interpretare il fenomeno dell'angolo di Brewster dal punto di vista microscopico. L'onda riflessa è prodotta dall'interferenza costruttiva delle emissioni dei dipoli della materia, eccitati dai campi dell'onda rifratta. Se la direzione dell'onda rifratta forma un angolo $\alpha$ con la direzione dell'onda riflessa, l'irraggiamento dei dipoli deve avere interferenza costruttiva su questa direzione; ma se $\alpha=90\text{\textdegree}$ i dipoli oscillano parallelamente a (quella che dovrebbe essere) la direzione dell'onda riflessa e quindi, poichè un dipolo non emette nella direzione del proprio asse, non ci può essere onda riflessa. Risulta chiaro allora che il fenomeno dell'angolo di Brewster non si può avere nel caso di polarizzazione S: infatti in questo caso i dipoli oscillano ortogonalmente al piano d'incidenza e quindi non può mai succedere che la loro direzione sia parallela a quella dell'onda riflessa.

\subsection{Cenno all'ottica geometrica}

Andiamo ora a considerare il caso in cui la caratteristiche del mezzo variano molto lentamente rispetto alla lunghezza d'onda. Sia $\xi$ una grandezza qualsiasi che descrive i campi di un'onda (può essere una qualsiasi componente di \textbf{E} o \textbf{B}); sappiamo che non funziona più la soluzione del tipo 
\begin{equation}
\xi=\xi_{0}e^{i\mathbf{k\cdot x}-i\omega t+\varphi}
\label{eq:onda piana geometrica}
\end{equation}
Scriviamo in generale la seguente espressione per i campi
\[
\xi=\xi_{0}(\mathbf{x},t)e^{iS(\mathbf{x},t)}
\]
dove $S(x,t)$ è chiamato \emph{iconale}. In regioni piccole dello spazio e per brevi intervalli di tempo l'iconale si può sviluppare
in serie:
\[
S=S_{0}+\mathbf{x\cdot}\nabla S+t\frac{\partial S}{\partial t}
\]
confrontando questo sviluppo con la \eqref{eq:onda piana geometrica} viene naturale porre le seguenti definizioni:
\[
\mathbf{k}\equiv\nabla S\qquad\qquad\omega\equiv-\frac{\partial S}{\partial t}
\]
L'informazione che si ha a disposizione è l'equazione d'onda, da cui si ricava una relazione di dispersione \emph{locale }$\omega=\omega(\mathbf{k},\mathbf{x},t);$ tutto ciò ha senso solamente se l'indice di rifrazione varia poco su una distanza pari ad una lunghezza d'onda. Per semplicità la derivazione che segue è fatta in un caso unidimensionale; alla fine l'estensione tridimensionale sarà ovvia. Per le definizioni che abbiamo posto
\[
\left.\frac{\partial\omega}{\partial x}\right|_{t}=-\frac{\partial S}{\partial x\partial t}=-\left.\frac{\partial k}{\partial t}\right|_{x}
\]
ma si ha anche
\[
\left.\frac{\partial\omega}{\partial x}\right|_{t}=\left.\frac{\partial\omega}{\partial x}\right|_{k,t}+\frac{\partial k}{\partial x}\left.\frac{\partial\omega}{\partial k}\right|_{x,t}
\]
Ma ${\displaystyle \frac{{\displaystyle \partial\omega}}{\partial k}}$ è la velocità di gruppo ${\displaystyle v_{g}=\frac{dx}{dt}}$. Dal confronto delle due relazioni si ottiene l'uguaglianza
\[
\underbrace{\left.\frac{\partial k}{\partial t}\right|_{x}+v_{g}\frac{\partial k}{\partial x}}_{derivata\: convettiva\:\frac{dk}{dt}}=-\frac{\partial\omega}{\partial x}
\]
Mettendo tutto insieme troviamo le equazioni di Hamilton per i raggi
\[
\begin{cases}
{\displaystyle \frac{{\displaystyle dx}}{dt}}= & {\displaystyle {\displaystyle \frac{\partial\omega}{\partial k}}}\\
\\{\displaystyle \frac{dk}{dt}}= & -{\displaystyle {\displaystyle \frac{\partial\omega}{\partial x}}}
\end{cases}
\]
la generalizzazione tridimensionale è
\[
\begin{cases}
{\displaystyle \frac{{\displaystyle d\mathbf{r}}}{dt}}= & {\displaystyle \nabla_{\mathbf{k}}\omega}\\
\\{\displaystyle \frac{{\displaystyle d\mathbf{k}}}{dt}}= & {\displaystyle -\nabla_{\mathbf{r}}\omega}
\end{cases}
\]
Quindi, nota la relazione di dispersione $\omega=\omega(k,\mathbf{x},t)$ è possibile, risolvendo questo sistema di equazioni, determinare la traiettoria dei raggi di luce. E' interessante osservare come queste equazioni siano assolutamente analoghe alle equazioni canoniche della meccanica classica tramite le corrispondenze
\begin{eqnarray*}
\omega & \leftrightarrow & H\\
k_{i} & \leftrightarrow & p_{i}\\
r_{i} & \leftrightarrow & q_{i}
\end{eqnarray*}
Ora vediamo un esempio di applicazione di quanto trovato.

\subsubsection*{Miraggi}

Consideriamo un mezzo la cui relazione di dispersione sia data da
\[
\omega=\frac{kc}{n(z)}
\]
in cui cioè l'indice di rifrazione dipende solo dalla coordinata $z$. Il sistema da risolvere è allora
\[
\begin{cases}
\dot{x}= & \frac{k_{x}c}{kn(z)}\\
\dot{z}= & \frac{k_{z}c}{kn(z)}\\
\dot{k}_{x}= & 0\\
\dot{k}_{z}= & \frac{kc}{n^{2}(z)}\frac{dn}{dz}
\end{cases}
\]
Si ricava allora ${\displaystyle \frac{dz}{dx}=\frac{k_{z}}{k_{x}}}$. Derivando rispetto a $x$ si ottiene
\[
\frac{d^{2}z}{dx^{2}}=\frac{1}{k_{x}}\frac{dk_{z}}{dx}=\frac{1}{k_{x}}\frac{dk_{z}}{dt}\frac{dt}{dx}=\frac{1}{2c^{2}k_{x}^{2}}\frac{k^{2}c^{2}}{n^{2}}\frac{d}{dz}n^{2}(z)
\]
L'equazione che descrive la traiettoria è dunque
\begin{equation}
\frac{d^{2}z}{dx^{2}}=\frac{\omega^{2}}{2c^{2}k_{x}^{2}}\frac{d}{dz}n^{2}(z)
\label{eq:eq_raggi}
\end{equation}
Utilizziamo questa equazione per dare una spiegazione del fenomeno dei miraggi: assumiamo (in modo un po' semplicistico) che l'indice di rifrazione dell'atmosfera vari secondo la legge
\[
n^{2}(z)=n_{0}^{2}(1+\alpha z)
\]
inserendo quest'ultima nella \eqref{eq:eq_raggi} si ottiene che la traiettoria dei raggi di luce è una parabola e questo dà una giustificazione al fenomeno dei miraggi.


\section{Diffrazione}

Vogliamo discutere il comportamento delle onde che vengono fatte passare attraverso un foro (diaframma). Come ricaveremo l'onda si sparpaglia; il parametro che conta è $D/\lambda$, dove $D$ è la dimensione caratteristica del diaframma. Per semplicità supporremo che il nostro diaframma sia una fessura, così il problema si riduce ad un caso bidimensionale; non succede nulla lungo $\hat{\mathbf{z}}$ (si veda la figura \ref{fig:Allargamento-del-pacchetto}). 

Supponiamo di avere all'istante $t=0$ un pacchetto d'onde di forma gaussiana:
\begin{equation}
E(x,y,0)=E_{0}\exp\left[-\left(\frac{x^{2}}{L^{2}}+\frac{y^{2}}{D^{2}}\right)+ik_{0}x\right]
\label{eq:gaussiana in x}
\end{equation}
In realtà stiamo supponendo che la nostra fessura sia ``qualcosa'' che costringe il pacchetto ad assumere una forma gaussiana. Inoltre supponiamo che $L\gg D$ (ovvero il pacchetto ``dura'' di più lungo $x$) o meglio che $k_{0}L\gg1$, cioè un pachetto che abbia molte oscillazioni all'interno. In trasformata di Fourier il pacchetto avrà la forma:
\begin{gather*}
\tilde{E}(k_{x},k_{y},0)=\frac{1}{2\pi}\iint_{-\infty}^{+\infty}E_{0}\exp\left[-\left(\frac{x^{2}}{L^{2}}+\frac{y^{2}}{D^{2}}\right)+ik_{0}x\right]\exp\left[-i\left(k_{x}x+k_{y}y\right)\right]dxdy
\end{gather*}
\[
=\frac{LD}{2}\exp\left[-\frac{\left(k_{x}-k_{0}\right)^{2}L^{2}}{4}-\frac{k_{y}^{2}D^{2}}{4}\right]
\]
cioè possiamo riscrivere, grazie all'inversione della trasformata di Fourier, la \eqref{eq:gaussiana in x} come
\begin{equation}
E(x,y,0)=\frac{1}{2\pi}\frac{LD}{2}\iint_{-\infty}^{+\infty}\exp\left[-\frac{\left(k_{x}-k_{0}\right)^{2}L^{2}}{4}-\frac{k_{y}^{2}D^{2}}{4}\right]\exp\left[i\left(k_{x}x+k_{y}y\right)\right]dk_{x}dk_{y}
\end{equation}
adesso su questa possiamo fare l'evoluzione temporale visto che sappiamo come evolvono le onde piane e dunque una loro qualsiasi sovrapposizione\footnote{Il lettore avrà notato la somiglianza con quello che si è fatto nella sezione \ref{sec:Onde-dispersive}. Non è un caso: in effetti stiamo facendo proprio la stessa cosa, soltanto in due dimensioni piuttosto che una.}. A questo scopo scriviamo (da adesso in poi trascuriamo i fattori costanti) 
\begin{equation}
E(x,y,t)\propto\iint_{-\infty}^{+\infty}\exp\left[-\frac{\left(k_{x}-k_{0}\right)^{2}L^{2}}{4}-\frac{k_{y}^{2}D^{2}}{4}\right]\exp\left[i\left(k_{x}x+k_{y}y\right)-i\omega(k)t\right]dk_{x}dk_{y}
\label{eq:evoluto-temporale}
\end{equation}
dove $\omega(k)=c\sqrt{k_{x}^{2}+k_{y}^{2}}$ . 

Per risolvere questo problema facciamo un ulteriore ipotesi, supponiamo che $k_{0}D\gg1$, cioè il diaframma sia molto maggiore della lunghezza d'onda lungo $x$; allora possiamo sviluppare $\omega(k)$ in modo da fattorizzare i due integrali in $k_{x}$ e $k_{y}$, questo se la gaussiana è molto piccata intorno a $k_{0}$ nelle $x$. Questa approssimazione equivale a prendere piccoli angoli di diffrazione; allora
\[
\omega(k)=c\sqrt{k_{x}^{2}+k_{y}^{2}}\cong c\left(k_{x}+\frac{k_{y}^{2}}{2k_{x}}\right)\cong c\left(k_{x}+\frac{k_{y}^{2}}{2k_{0}}\right)
\]
Sostituendo quest'ultima nella \eqref{eq:evoluto-temporale} otteniamo
\[
\int_{-\infty}^{+\infty}\exp\left[-\frac{\left(k_{x}-k_{0}\right)^{2}L^{2}}{4}\right]\exp\left[ik_{x}\left(x-ct\right)\right]dk_{x}\int_{-\infty}^{+\infty}\exp\left[-\left(\frac{D^{2}}{4}+i\frac{ct}{2k_{0}}\right)k_{y}^{2}\right]\exp\left[ik_{y}y\right]dk_{y}
\]
il primo integrale è un pacchetto che si propaga senza disperdersi (nelle ipotesi che stiamo considerando), quindi concetriamoci sul
secondo: integrando si ottiene
\begin{equation}
\frac{1}{\sqrt{D^{2}+2i\frac{ct}{k_{0}}}}\exp\left[-\frac{y^{2}}{\left(D^{2}+2i\frac{ct}{k_{0}}\right)}\right]
\end{equation}
dunque, prendendone la parte reale, si ha che l'allargamento del pacchetto (nelle $y$) è
\[
\mathcal{L}=\frac{1}{D}\sqrt{D^{4}+4\left(\frac{ct}{k_{0}}\right)^{2}}=\sqrt{D^{2}+4\left(\frac{X(t)}{Dk_{0}}\right)^{2}}
\]
dove $X(t)$ è la distanza percorsa lungo $x$. In definitiva il nostro pacchetto, che al tempo $t=0$ era dato dalla \eqref{eq:gaussiana in x}, dopo un tempo $t$ è
\begin{wrapfigure}{O}{0.4\columnwidth}%
\centering
\includegraphics[scale=0.8]{diffrazionen}
\caption{Rappresentazione schematica del problema della diffrazione\label{fig:Allargamento-del-pacchetto}}
\end{wrapfigure}%
\[
E(x,y,t)\propto\exp\left[-ik_{0}\left(x-ct\right)-\frac{\left(x-ct\right)^{2}}{L^{2}}\right]\frac{\exp\left[-\frac{y^{2}}{\left(D^{2}+2i\frac{ct}{k_{0}}\right)}\right]}{\sqrt{D^{2}+2i\frac{ct}{k_{0}}}}
\]

A che distanza il nostro pacchetto si allarga di un fattore due, $\mathcal{L}\rightarrow2\mathcal{L}$?

Basta imporre che 
\[
2D=\sqrt{D^{2}+4\left(\frac{X(t)}{Dk_{0}}\right)^{2}}\qquad\Rightarrow\qquad X^{2}=\frac{3}{4}D^{4}k_{0}^{2}
\]
quindi si ottiene per 
\[
X\approx D^{2}k_{0}
\]
questa è la cosidetta \emph{lunghezza di Rayleigh}.

Inoltre dalla figura \ref{fig:Allargamento-del-pacchetto} si vede che \emph{l'angolo di diffrazione} è: 
\[
\theta\approx\frac{D}{X}=\frac{D}{D^{2}k_{0}}=\frac{1}{k_{0}D}=\frac{\lambda}{D}
\]
ritroviamo il risultato che avevamo preannucciato all'inizio.


\section{Introduzione all'interferenza}

Il fenomeno dell'interferenza è espressione diretta del principio di sovrapposizione. Esistono molte situazioni in cui si ha a che fare con fenomeni di interferenza. Consideriamo ad esempio la situazione descritta in figura \ref{fig:Doppia-fenditura}: della luce prodotta da \emph{una} sorgente viene fatta passare attraverso due fori e viene poi raccolta su uno schermo. E' facile, usando il principio di sovrapposizione, mostrare che in base alla differenza di cammino ottico $\delta$ si determina uno sfasamento $\Delta\phi=k\delta$ per cui vi sono punti in cui l'interferenza è costruttiva e altri punti in cui l'interferenza è distruttiva:
\begin{wrapfigure}{o}{0.4\columnwidth}%
\centering
\includegraphics[scale=0.7]{interferenza}
\caption{Doppia fenditura\label{fig:Doppia-fenditura}}
\end{wrapfigure}%
\begin{eqnarray*}
\Delta\phi & = & 2n\pi\qquad\:\rightarrow \emph{\text{interferenza costruttiva}}\\
\Delta\phi & = & (2n+1)\pi\,\rightarrow  \emph{\text{interferenza distruttiva}}
\end{eqnarray*}
Si osserva quindi sullo schermo la caratteristica figura a bande più e meno illuminate (frange d'interferenza). Vediamo quali sono le condizioni necessarie per osservare l'interferenza. Innanzitutto le sorgenti devono essere il più possibile monocromatiche e devono avere la \emph{stessa} frequenza; infatti se le due onde avessero frequenze $\omega_{1}\neq\omega_{2}$, il vettore di Poynting (come abbiamo visto a pagina \pageref{somma_poynting}) avrebbe dei termini di frequenza $\omega_{1}+\omega_{2}$ e $\omega_{1}-\omega_{2}$ che, mediati nel tempo, si annullano e l'intensità osservata sullo schermo si ridurebbe alla somma delle intensità delle onde incidenti. Inoltre la radiazione deve essere \emph{coerente}, cioè la differenza di fase tra le onde che interferiscono deve essere ben definita e costante; se ciò non accade, come si è visto quando abbiamo parlato del colore del cielo, i termini di interferenza si mediano statisticamente a zero. Si osserva poi che la differenza di cammino ottico deve essere minore di una certa lunghezza $l_{c}$: infatti dopo un certo intervallo temporale, e quindi spaziale, non si ha più controllo sulla fase dei due segnali e la radiazione perde coerenza. Questa distanza $l_{c}$ è chiamata \emph{lunghezza di correlazione}\footnote{L'argomento sarà approfondito in corsi successivi.}.
