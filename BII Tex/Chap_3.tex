\chapter{Formulazione covariante dell'elettromagnetismo}

\subsubsection*{Notazione}

In seguito useremo la seguente notazione: non si farà distinzione tra quadrivettori covarianti e controvarianti, di conseguenza gli
indici saranno posti tutti in basso; un qualsiasi quadrivettore verrà scritto con la componente temporale alla fine, cioè $a_{\mu}=(a_{x},a_{y},a_{z},a_{0})=(\mathbf{a},a_{0})$. Inoltre useremo la seguente metrica
\[
g_{\mu\nu}=\left(
\begin{array}{cccc}
1\\
 & 1\\
 &  & 1\\
 &  &  & -1
\end{array}\right)
\]
con la notazione che, se $a_{\mu}=(\mathbf{a},a_{0})$ e $b_{\mu}=(\mathbf{b},b_{0})$, i prodotti scalari sono dati da 
\[
a_{\mu}b_{\mu}=a_{\mu}g_{\mu\nu}b_{\nu}=\mathbf{a}\cdot\mathbf{b}-a_{0}b_{0}
\]
dove utilizziamo la convenzione di Einstein, ovvero si deve sommare sugli indici ripetuti.


\section{Richiami di relatività}

Definiamo adesso le quantità cinematiche e poi deriveremo le espressioni per gli operatori differenziali. Definiamo, conseguentemente alla nostra convezione, il quadrivettore posizione
\[
x_{\mu}=(\mathbf{x},ct)
\]
Come sappiamo i quadrivettori si trasformano, per cambio di sistema di riferimento, tramite le trasformazioni di Lorentz; supponendo che $\mathcal{S}$ sia il sistema di riferimento del laboratorio e $\mathcal{S}'$ sia un sistema di riferimento in moto rispetto a $\mathcal{S}$ con velocita $\mathbf{v}$, con $\mathbf{v}$ lungo $x$ si hanno le seguenti espressioni
\begin{gather}
\left\{ 
\begin{aligned}
x'= & {\displaystyle \frac{x-\beta ct}{\sqrt{1-\beta^{2}}}}=\gamma(x-\beta ct)\\
y'= & y\\
z'= & z\\
ct'= & {\displaystyle \frac{ct-\beta x}{\sqrt{1-\beta^{2}}}}=\gamma(ct-\beta x)
\end{aligned}
\right.
\end{gather}
dove 
\[
\begin{aligned}
\beta=\frac{v}{c} & \qquad\,\, & \gamma=\frac{1}{\sqrt{1-\beta^{2}}}
\end{aligned}
\]
In forma matriciale il boost di Lorentz è dato da
\[
\Lambda_{\mu\nu}=\left(
\begin{array}{cccc}
\gamma & 0 & 0 & -\gamma\beta\\
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
-\gamma\beta & 0 & 0 & \gamma
\end{array}
\right)
\]
allora il quadrivettore trasformato sarà
\[
x_{\mu}'=\Lambda_{\mu\nu}x_{\nu}
\]
Inoltre, si definisce quadrivelocità
\[
u_{\mu}=\frac{dx_{\mu}}{d\tau}
\]
dove $\tau$ è il tempo proprio. Ricordando la proprietà ${\displaystyle \frac{dt}{d\tau}=\gamma}$, si trova facilmente che la quadrivelocità è espressa da
\[
u_{\mu}=(\gamma\mathbf{u},\gamma c)
\]
con $\mathbf{u}$ velocità della particella. Data la quadrivelocità è intuitivo definire il quadrimpulso $p_{\mu}$ nel seguente modo
\[
p_{\mu}=m_{0}u_{\mu}=\left(m_{0}\gamma\mathbf{u},m_{0}\gamma c\right)
\]
dove $m_{0}$ è la massa a riposo della particella. Inoltre dalla celeberrima relazione $E=mc^{2}=m_{0}\gamma c^{2}$, si può riscrivere $p_{\mu}$ come
\[
p_{\mu}=\left(\mathbf{p},\frac{E}{c}\right)
\]
dove si è indicato $\mathbf{p}=m_{0}\gamma\mathbf{u}$.

\subsection{Operatori differenziali}

Per dare una veste covariante alle equazioni di Maxwell ci servono le opportune generalizzazioni 3+1-dimensionali dei vari operatori
differenziali. Cominciamo dal gradiente: sfruttiamo la proprietà che le quantità scalari sono, per definizione, invarianti per cambio di sistema di riferimento; prendiamo come quantità scalare $\phi$ e supponiamo di avere due sistemi uno in moto rispetto all'altro, con la convezione che si è fatta per scrivere le trasformazioni di Lorentz. Facendo una variazione infinitesima di $\phi$, si ha
\begin{equation}
\begin{aligned}
d\phi=\frac{\partial\phi}{\partial x}dx+\frac{\partial\phi}{\partial t}dt & \quad & \text{in}\,\,\mathcal{S} & \qquad & d\phi=\frac{\partial\phi}{\partial x'}dx'+\frac{\partial\phi}{\partial t'}dt' & \quad & \text{in}\,\,\mathcal{S}'
\end{aligned}
\label{eq:phi e phi1}
\end{equation}
ma dalle trasformazioni di Lorentz 
\begin{align*}
dx'= & \gamma\left(dx-\beta cdt\right)\\
cdt'= & \gamma\left(cdt-\beta dx\right)
\end{align*}
così visto che i due termini della \eqref{eq:phi e phi1} devono essere uguali, poiché come abbiamo detto sono quantità scalari, si ha, sostituendo i differenziali appena trovati
\begin{eqnarray}
d\phi=\frac{\partial\phi}{\partial x}dx+\frac{\partial\phi}{\partial t}dt & = & \frac{\partial\phi}{\partial x'}dx'+\frac{\partial\phi}{\partial t'}dt'\nonumber \\
 & = & \frac{\partial\phi}{\partial x'}\left[\gamma\left(dx-\beta cdt\right)\right]+\frac{\partial\phi}{\partial t'}\left[\gamma\left(cdt-\beta dx\right)\right]\nonumber \\
 & = & \left[\gamma\left(\frac{\partial\phi}{\partial x'}-\beta\frac{\partial\phi}{\partial t'}\right)\right]dx+\left[\gamma\left(\frac{\partial\phi}{\partial t'}-\beta\frac{\partial\phi}{\partial x'}\right)\right]cdt
\label{eq:quadrigradientre}
\end{eqnarray}
Dovendo essere il quadrigradiente di qualcosa un quadrivettore, deve trasformare tramite le trasformazioni di Lorentz; sembra che non torni il segno nella \eqref{eq:quadrigradientre}, ma le cose si aggiustano ponendo che il quadrigradiente sia:
\[
\partial_{\mu}=\left(\nabla,-\frac{\partial}{\partial ct}\right)
\]
che è covariante. Analogamente si definisce la quadridivergenza come
\[
\partial_{\mu}a_{\mu}=\nabla\cdot\mathbf{a}+\frac{\partial a_{0}}{\partial ct}
\]
E quindi il laplaciano come
\[
\partial_{\mu}\partial_{\mu}=\nabla^{2}-\frac{1}{c^{2}}\frac{\partial^{2}}{\partial t^{2}}
\]
che viene spesso chiamato D'Alembertiano e si indica $\square\equiv\partial_{\mu}\partial_{\mu}$.


\section{Equazioni di Maxwell in forma covariante}

Per una formulazione covariante delle equazioni di Maxwell è bene partire da un invariante: la carica. Sperimentalmente si vede che
la carica si conserva e, come sappiamo, rispetta l'equazione di continuità
\[
\nabla\mathbf{j}+\frac{\partial\rho}{\partial t}=0
\]
che può essere riscritta nella forma
\[
\partial_{\mu}j_{\mu}=0
\]
Visto che a secondo membro abbiamo un invariante (zero), anche al primo membro deve esserci un invariante. Dato che $\partial_{\mu}$
è un vettore covariante, l'unico modo per fare un invariante è che $j_{\mu}$ sia un quadrivettore. Si definisce allora la \emph{quadricorrente}
\[
j_{\mu}=\left(\mathbf{j},c\rho\right)
\]
A questo punto la formulazione covariante non è altro che una riscrittura delle equazioni dell'elettromagnetismo. Sappiamo che per i potenziali valgono le equazioni 
\begin{eqnarray*}
\nabla^{2}\varphi-\frac{1}{c^{2}}\frac{\partial^{2}\varphi}{\partial t^{2}} & = & -\frac{\rho}{\varepsilon_{0}}\\
\nabla^{2}c\mathbf{A}-\frac{1}{c^{2}}\frac{\partial^{2}c\mathbf{A}}{\partial t^{2}} & = & -\frac{\mathbf{j}}{\varepsilon_{0}c}
\end{eqnarray*}
dove abbiamo moltiplicato la seconda per $c$; riscrivendole come segue
\[
\begin{aligned}
\square c\mathbf{A}=-\frac{\mathbf{j}}{\varepsilon_{0}c} & \qquad & \qquad & \square\varphi=-\frac{c\rho}{\varepsilon_{0}c}
\end{aligned}
\]
ci si accorge subito che i potenziali sono le componenti di un quadrivettore in quanto lo sono le sorgenti; avremo
\begin{equation}
\partial_{\mu}\partial_{\mu}A_{\nu}=-\frac{j_{\nu}}{\varepsilon_{0}c}
\label{eq:maxwellCovariante}
\end{equation}
dove $A_{\mu}$ è il \emph{quadripotenziale} di componenti
\[
A_{\mu}=\left(c\mathbf{A},\varphi\right)
\]
Inoltre la gauge Lorenz può essere riscritta nel formalismo che stiamo usando nel seguente modo: 
\begin{equation}
\partial_{\mu}A_{\mu}=0
\label{eq:gauge Covariante}
\end{equation}
Si arriva subito alla \eqref{eq:gauge Covariante} riscrivendo la \eqref{eq:gauge} nel seguente modo $\nabla\cdot c\mathbf{A}+\frac{\partial\varphi}{\partial ct}=0$. Si vede altrettanto bene che la nuova invarianza di gauge è 
\[
A_{\mu}'=A_{\mu}+\partial_{\mu}(c\psi)
\]
basta ricordarsi che la trasformazione di gauge è data da
\[
\begin{cases}
\mathbf{A} & \rightarrow\mathbf{A'}=\mathbf{A}+\nabla\psi\\
\varphi & \rightarrow\varphi'=\varphi-\frac{\partial\psi}{\partial t}
\end{cases}
\]
Sorge una domanda: se siamo fuori gauge di Lorenz come sono le equazioni covarianti? Si trova facilmente partendo dalle equazioni per i potenziali che
\begin{equation}
\partial_{\mu}\partial_{\mu}A_{\nu}-\partial_{\mu}\partial_{\nu}A_{\mu}=\partial_{\mu}F_{\mu\nu}=-\frac{j_{\nu}}{\varepsilon_{0}c}
\label{eq:disomogene-covariante}
\end{equation}
dove $F_{\mu\nu}=\partial_{\mu}A_{\nu}-\partial_{\nu}A_{\mu}$ è detto \emph{tensore dei campi}. $F_{\mu\nu}$ è un tensore antisimmetrico a due indici che (ricordando l'espressione di $A_{\mu}$ e come i campi sono legati ai potenziali) è espresso dalla seguente matrice:
\begin{equation}
F_{\mu\nu}=\left(
\begin{array}{cccc}
0 & cB_{z} & -cB_{y} & -E_{x}\\
-cB_{z} & 0 & cB_{x} & -E_{y}\\
cB_{y} & -cB_{x} & 0 & -E_{z}\\
E_{x} & E_{y} & E_{z} & 0
\end{array}
\right)
\end{equation}
Tramite il tensore dei campi abbiamo scritto le equazioni disomogenee in forma covariante (equazione \eqref{eq:disomogene-covariante}), quelle omogenee si possono scrivere in forma covariante nel seguente modo
\begin{equation}
\partial_{\mu}\widetilde{F}_{\mu\nu}=0
\end{equation}
dove $\widetilde{F}_{\mu\nu}$ è un altro tensore, detto duale di $F_{\mu\nu}$, definito dalla seguente relazione: 
\[
\widetilde{F}_{\alpha\beta}=\frac{1}{2}\epsilon_{\alpha\beta\mu\nu}F_{\mu\nu}
\]
dove $\epsilon_{\alpha\beta\mu\nu}$ è l'ovvia generalizazzione del tensore $\epsilon_{ijk}$ in quattro dimensioni, ovvero
\[
\epsilon_{\alpha\beta\gamma\delta}=\left\{ 
\begin{array}{ccc}
1 &  & \mbox{per combinazione ciclica degli indici}\\
0 &  & \mbox{se almeno due degli indici sono uguali}\\
-1 &  & \mbox{per combinazioni non cicliche}
\end{array}
\right.
\]
Questi tensori, oltre a dirci come trasformano i campi (lo faremo più avanti), ci danno i seguenti invarianti relativistici
\begin{eqnarray}
F_{\mu\nu}F_{\mu\nu} & = & 2\left(E^{2}-c^{2}B^{2}\right)\label{eq:E-cB}\\
F_{\mu\nu}\widetilde{F}_{\mu\nu} & = & 4\mathbf{E}\cdot\mathbf{B}\label{eq:E*B}
\end{eqnarray}
La \eqref{eq:E-cB} e la \eqref{eq:E*B} ci dicono per esempio che se in un'onda il campo elettrico e magnetico sono ortogonali tra di
loro lo sono in qualsiasi altro sistema di riferimento (SR); inoltre se esiste un SR in cui $\mathbf{E}\cdot\mathbf{B}=0$ e 
\begin{itemize}
\item $E^{2}<c^{2}B^{2}$ allora esiste un SR in cui il campo elettrico è nullo, $\mathbf{E}=0$.
\item $E^{2}>c^{2}B^{2}$ allora esiste un SR in cui il campo magnetico è nullo, $\mathbf{B}=0$.
\end{itemize}

\section{Trasformazioni per cambio di sistema di riferimento}


\subsection{Trasformazione dei potenziali}

Per avere una trattazione completa dell'elettromagnetismo dobbiamo vedere come si trasformano campi e potenziali. Per quest'ultimi la cosa diviene banale in quanto, essendo $A_{\mu}$ un quadrivettore, le sue componenti trasformano tramite le trasformazioni di Lorentz:
\[
\left\{ 
\begin{aligned}cA_{x}' & =\gamma{\displaystyle \left(cA_{x}-\beta\varphi\right)}\\
cA_{y,z}' & =cA_{y,z}\\
\varphi' & =\gamma\left(\varphi-\beta cA_{x}\right)
\end{aligned}
\right.
\]
Prima di passare ai campi diamo un esempio dell'utilizzo di queste trasformazioni nel caso di una carica in moto uniforme, proprio come si è fatto nella sezione \ref{sec:Potenziali-di-Lienard-Wiechert} tramite le soluzioni di Liénard-Wiechert. Sia il sistema $\mathcal{S}$ quello del laboratorio in cui la carica si muove con velocità $\mathbf{v}=v\hat{\mathbf{x}}$ e $\mathcal{S}'$ sia il sistema solidale con la carica. In $\mathcal{S}'$ si hanno i potenziali\footnote{Qui gli elementi primati indicano che siamo nel riferimento $\mathcal{S}'$ e, diversamente dalla sezione \ref{sec:Potenziali-di-Lienard-Wiechert}, non che sono calcolati al tempo ritardato}
\[
\begin{aligned}
\varphi'(r')=\frac{q}{4\pi\varepsilon_{0}r'} & \qquad & \qquad & \mathbf{A}'=0
\end{aligned}
\]
Per calcolare il potenziale in $\mathcal{S}$ basta applicare le trasformazioni di Lorentz inverse; si trova
\[
\varphi(r')=\gamma\varphi'(r')=\gamma\frac{q}{4\pi\varepsilon_{0}r'}
\]
si noti che il potenziale è espresso in funzione delle cordinate di $\mathcal{S}'$, ma a noi interessa il potenziale come funzione delle coordinate di $\mathcal{S}$; dobbiamo sostituire a $r'$ la sua espressione in funzione di $(r,\, t)$. Visto che 
\[
r'=\sqrt{x'^{2}+y'^{2}+z'^{2}}=\sqrt{\gamma^{2}(x-vt)^{2}+y^{2}+z^{2}}
\]
si ha che il potenziale di una carica in moto con velocità uniforme è:
\begin{equation}
\varphi(r,t)=\frac{q}{4\pi\varepsilon_{0}}\frac{\gamma}{\sqrt{\gamma^{2}(x-vt)^{2}+y^{2}+z^{2}}}
\label{eq:pot come lineard}
\end{equation}
Confrontando la \eqref{eq:pot come lineard} con la \eqref{eq:pot di lineard} si vede che le due derivazioni sono coerenti, come è giusto che sia. Facciamo notare che in $\mathcal{S}$ compare un potenziale vettore assente in $\mathcal{S}'$; lasciamo al lettore la facile derivazione.


\subsection{Trasformazione dei campi}

Adesso andiamo a vedere come si trasformano i campi; la cosa non è così facile come per i potenziali in quanto come abbiamo visto i campi non sono le componenti di un quadrivettore, ma di un tensore a 2 indici antisimmetrico. Si possono seguire due strade: una è quella di trasformare i potenziali e poi da essi ricavare i campi, ma è abbastanza ``contosa''; la seconda è quella di trasformare il tensore le cui nuove componenti saranno i campi trasformati. 

Alla fine si trovano le seguenti trasformazioni, per un moto lungo l'asse $\hat{\mathbf{x}}$ con velocità $v$: 
\begin{align*}
E'_{x} & =E_{x} &  &  & B'_{x} & =B_{x}\\
E'_{y} & =\gamma\left(E_{y}-vB_{z}\right) &  &  & B'_{y} & =\gamma\left({\displaystyle B_{y}+\frac{v}{c^{2}}E_{z}}\right)\\
E'_{z} & =\gamma\left(E_{z}+vB_{y}\right) &  &  & B'_{z} & =\gamma{\displaystyle \left({\displaystyle B_{z}-\frac{v}{c^{2}}E_{y}}\right)}
\end{align*}
Queste formule possono essere riscritte in un'altra forma talvolta più utile: 
\begin{align*}
E'_{\parallel} & =E_{\parallel} &  &  & B'_{\parallel} & =B_{\parallel}\\
E'_{\perp} & =\gamma\left(\mathbf{E}+\mathbf{v}\times\mathbf{B}\right)_{\perp} &  &  & B'_{\perp} & =\gamma{\displaystyle \left(\mathbf{B}-\frac{\mathbf{v}\times\mathbf{E}}{c^{2}}\right)_{\perp}}
\end{align*}
Nella figura seguente sono riportate le immagini del campo elettrico di una carica in movimento con tre diverse velocità. 
\begin{figure}[H]
\begin{centering}
\subfloat[$\beta=0$]{
\includegraphics[scale=0.65]{CampoPerV=0}
}~~~~~~~%
\subfloat[$\beta=0.6$]{
\centering
\includegraphics[scale=0.8]{CampoPerV=06fg}
}~~~~~~~~~%\
\subfloat[$\beta=0.9$]{
\begin{raggedleft}
\includegraphics[scale=0.85]{CampoPerV=09fg}
\end{raggedleft}
}
\par
\end{centering}
\caption{Campo elettrico di una carica in moto uniforme per vari valori di $\beta$ }
\end{figure}



\section{Dinamica relativistica}

Per chiudere il quadro della formulazione covariante dell'elettromagnetismo dobbiamo sapere come il campo interagisce con le cariche. In regime non relativistico la legge del moto di una carica è 
\[
\frac{d\mathbf{p}}{dt}=q\left(\mathbf{E}+\mathbf{v}\times\mathbf{B}\right)
\]
dove l'espressione a secondo membro è la ben nota forza di Lorentz. Ora in ambito relativistico sappiamo che il momento viene modificato tramite un fattore $\gamma$, così che 
\begin{equation}
\frac{d}{dt}\left(\gamma m_{0}\mathbf{v}\right)=\frac{d}{dt}\left(\frac{m_{0}\mathbf{v}}{\sqrt{1-v^{2}/c^{2}}}\right)=q\left(\mathbf{E}+\mathbf{v}\times\mathbf{B}\right)
\label{eq:Lorentz relativitica}
\end{equation}
ma sappiamo che per avere un'espressione covariante dobbiamo per forza usare il tensore dei campi piuttosto che i campi stessi. Otteremo un'equazione del tipo
\[
\mathcal{F}_{\mu}=\frac{dp_{\mu}}{dt}
\]
dove $\mathcal{F}_{\mu}$ è la quadriforza, che in componenti è $\mathcal{F}_{\mu}=\left(\gamma\mathbf{F},\frac{\gamma}{c}\mathbf{F}\cdot\mathbf{v}\right)$.

L'equazione del moto in forma covariante è espressa da
\begin{equation}
\frac{dp_{\mu}}{dt}=m_{0}\frac{d^{2}x_{\mu}}{dt^{2}}=qF_{\mu\nu}u_{\nu}
\label{eq:del moto covariante}
\end{equation}
Svolgendo i calcoli dell'espressione sopra ci si accorge che le componenti spaziali danno proprio la~\eqref{eq:Lorentz relativitica}.

Diamo, infine, un cenno al cosidetto \emph{tensore energia-impulso}. Si sono ricavati due teoremi che esprimevano uno la conservazione dell'energia, l'altro la conservazione dell'impulso. Questi due possono essere riscritti in forma covariante introducendo il seguente tensore
\[
\Theta_{\alpha\beta}=\left(
\begin{array}{ccc|c}
 &  &  & \,\\
 & -\frac{1}{c}T_{ij} &  & \frac{1}{c}\mathbf{S}\\
 &  &  & \,\\
\hline  & \frac{1}{c}\mathbf{S} &  & u
\end{array}
\right)
\]
dove $T_{ij}$ è il tensore degli stress di Maxwell, $\mathbf{S}$ è il vettore di Poynting e $u$ è la densità di energia del campo
elettromagnetico. Allora le suddette leggi di conservazione, in assenza di sorgenti, sono date da
\[
\partial_{\alpha}\Theta_{\alpha\beta}=0
\]
La cui componente temporale dà il teorema di Poynting e la parte spaziale quello dell'impulso.

Se poi si vuole aggiungere anche il contributo delle sorgenti non è difficile vedere che l'espressione si modifica nella seguente maniera
\[
\partial_{\alpha}\Theta_{\alpha\beta}=-\frac{1}{c}F_{\alpha\beta}j_{\alpha}
\]
Quindi l'equazione sopra esprime in modo covariante la conservazione dell'energia e della quantità di moto del campo elettromagnetico.
