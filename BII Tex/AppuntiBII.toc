\select@language {italian}
\contentsline {chapter}{\numberline {1}Oscillazioni e onde}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}La corda vibrante}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Derivazione dell'equazione}{5}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Le soluzioni dell'equazione della corda vibrante}{6}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Battimenti}{7}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Trasformata di Fourier}{8}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Riflessione e onde stazionarie}{12}{subsection.1.1.5}
\contentsline {subsection}{\numberline {1.1.6}Conservazione dell'energia}{14}{subsection.1.1.6}
\contentsline {section}{\numberline {1.2}Onde 3-dimensionali}{15}{section.1.2}
\contentsline {section}{\numberline {1.3}Onde dispersive}{16}{section.1.3}
\contentsline {chapter}{\numberline {2}Elettromagnetismo nel vuoto}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Potenziali elettromagnetici}{19}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Gauge di Lorenz}{20}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Equazioni per i campi}{21}{section.2.2}
\contentsline {section}{\numberline {2.3}Soluzioni delle equazioni di Maxwell}{23}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Potenziali ritardati}{25}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Sviluppo in multipoli }{26}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Dipolo elettrico}{27}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Quadrupolo elettrico e dipolo magnetico }{29}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}Teorema di Poynting}{31}{section.2.5}
\contentsline {section}{\numberline {2.6}Irraggiamento}{34}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Irraggiamento di dipolo}{34}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Irraggiamento di dipolo magnetico e quadrupolo}{35}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}Diffusione}{36}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Perch\IeC {\'e} il cielo \IeC {\`e} azzurro}{37}{subsection.2.7.1}
\contentsline {section}{\numberline {2.8}Densit\IeC {\`a} di impulso del campo elettromagnetico}{39}{section.2.8}
\contentsline {section}{\numberline {2.9}Potenziali di Li\IeC {\'e}nard-Wiechert}{41}{section.2.9}
\contentsline {chapter}{\numberline {3}Formulazione covariante dell'elettromagnetismo}{45}{chapter.3}
\contentsline {section}{\numberline {3.1}Richiami di relativit\IeC {\`a}}{45}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Operatori differenziali}{46}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Equazioni di Maxwell in forma covariante}{47}{section.3.2}
\contentsline {section}{\numberline {3.3}Trasformazioni per cambio di sistema di riferimento}{49}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Trasformazione dei potenziali}{49}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Trasformazione dei campi}{50}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Dinamica relativistica}{51}{section.3.4}
\contentsline {chapter}{\numberline {4}Onde EM nei mezzi}{53}{chapter.4}
\contentsline {section}{\numberline {4.1}Onde elettromagnetiche nei dielettrici}{53}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Modello fisico}{53}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Equazioni di Maxwell nei dielettrici}{54}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Onde nei conduttori}{56}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Energia trasportata da un'onda evanescente}{58}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}Onde in mezzi disomogenei}{58}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Riflessione e rifrazione}{59}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}Condizioni di raccordo dei campi}{59}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}Le leggi della riflessione e della rifrazione}{60}{subsubsection.4.3.1.2}
\contentsline {subsubsection}{\numberline {4.3.1.3}Formule di Fresnel}{62}{subsubsection.4.3.1.3}
\contentsline {subsubsection}{\numberline {4.3.1.4}Angolo di Brewster}{62}{subsubsection.4.3.1.4}
\contentsline {subsection}{\numberline {4.3.2}Cenno all'ottica geometrica}{63}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Diffrazione}{65}{section.4.4}
\contentsline {section}{\numberline {4.5}Introduzione all'interferenza}{67}{section.4.5}
\contentsline {chapter}{\numberline {A}Calcolo indiciale}{69}{appendix.A}
