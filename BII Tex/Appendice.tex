\chapter{Calcolo indiciale}

Qui vorremmo riportare poche semplici regole per non ``perdersi'' nei calcoli con gli indici che si incontrano nel testo. In questa appendice indicheremo (per essere originali) con $\phi$ un qualsiasi campo scalare e con $\mathbf{A}$ un qualsiasi campo vettoriale; inoltre useremo la convenzione di Einstein, ovvero si deve sommare sugli indici ripetuti.

In notazione indiciale un qualsiasi vettore è dato da ($\mathbf{e}_{i}$ è una base ortonormale, in particolare $\mathbf{e}_{i}\cdot\mathbf{e}_{j}=\delta_{ij}$)
\[
\mathbf{a}=\sum_{i}a_{i}\mathbf{e}_{i}\quad\mbox{ che nella convezione di Einstein è }\,\mathbf{a}=a_{i}\mathbf{e}_{i}
\]
segue che il prodotto scalare è dato da
\begin{eqnarray*}
\mathbf{a}\cdot\mathbf{b} & = & \left(a_{i}\mathbf{e}_{i}\right)\cdot\left(b_{j}\mathbf{e}_{j}\right)\\
 & = & a_{i}b_{j}\mathbf{e}_{i}\cdot\mathbf{e}_{j}\\
 & = & a_{i}b_{j}\delta_{ij}\\
 & = & a_{i}b_{i}
\end{eqnarray*}
invece il prodotto vettore è (come dovrebbe essere noto)
\[
\mathbf{a}\times\mathbf{b}=\epsilon_{ijk}a_{j}b_{k}\mathbf{e}_{i}
\]
dove $\epsilon_{ijk}$ è il tensore di Ricci ed è un tensore completamente antisimmetrico 
\[
\epsilon_{ijk}=
\begin{cases}
1 & \mbox{se}\quad ijk=\left\{ 123,231,312\right\} \\
0 & \mbox{se due indici sono uguali}\\
-1 & \mbox{se}\quad ijk=\left\{ 132,321,213\right\} 
\end{cases}
\]
Passiamo a considerare operazioni differenziali: gradiente, divergenza, laplaciano...
\begin{itemize}
\item Gradiente $\nabla\phi=\partial_{i}\phi\mathbf{e}_{i}$
\item Divergenza $\nabla\cdot\mathbf{A}=\partial_{i}A_{i}$
\item Rotore $\nabla\times\mathbf{A}=\epsilon_{ijk}\partial_{j}A_{k}\mathbf{e}_{i}$
\item Laplaciano $\nabla^{2}\phi=\partial_{i}\partial_{i}\phi$
\end{itemize}
Dalle espressioni sopra e ricordando le seguenti semplici regole, possiamo fare svariati conti senza sbagliare:
\begin{itemize}
\item Ricordarsi di \underbar{non} usare lo stesso indice per quantità diverse, esempio:
\[
\nabla\times\left(\nabla\times\mathbf{A}\right)=
\begin{cases}
\epsilon_{ijk}\partial_{j}\left(\epsilon_{kjl}\partial_{j}A_{l}\right) & \mbox{ SBAGLIATA, l'indice j compare in due quantità diverse}\\
\epsilon_{ijk}\partial_{j}\left(\epsilon_{klm}\partial_{l}A_{m}\right) & \mbox{ OK}
\end{cases}
\]
\item Ricordarsi che $\epsilon_{ijk}$ sono dei numeri e, come tali, passano indenni le derivate.
\item Le derivate agiscono su tutto quello che c'è dopo, tranne quando ci sono le parentesi che ricordano su cosa agiscono. Inoltre commutano tra di loro.
\item Può servire la seguente identità, che non dimostriamo, 
\begin{equation}
\epsilon_{ijk}\epsilon_{ilm}=\delta_{jl}\delta_{km}-\delta_{jm}\delta_{kl}
\label{eq:EijkEilm}
\end{equation}
\end{itemize}
Adesso daremo degli esempi per fare un po' di pratica con questa notazione.

\subsubsection*{$\nabla\cdot\left(\nabla\times\mathbf{A}\right)=0$}

\begin{eqnarray*}
\nabla\cdot\left(\nabla\times\mathbf{A}\right) & = & \partial_{i}\epsilon_{ijk}\partial_{j}A_{k}\\
 & = & \epsilon_{ijk}\partial_{i}\partial_{j}A_{k}\\
 & = & 0
\end{eqnarray*}
Perchè $\epsilon_{ijk}$ è antisimmetrico nelle componenti mentre $\partial_{i}\partial_{j}$ è simmetrico, quindi la somma (stiamo utilizzando la convezione di Einstein $\Rightarrow$ si deve sommare su gli indici ripetuti) fa zero.

\subsubsection*{$\nabla\times\nabla\phi=0$}

\begin{eqnarray*}
\nabla\times\nabla\phi & = & \epsilon_{ijk}\partial_{j}\partial_{k}\phi\mathbf{e}_{k}=0
\end{eqnarray*}
E' nullo per lo stesso motivo di quello sopra, ovvero $\epsilon_{ijk}$ è antisimmetrico invece le derivate non lo sono, dunque la somma fa zero.

\subsubsection*{$\nabla\times\left(\nabla\times\mathbf{A}\right)=\nabla\left(\nabla\cdot\mathbf{A}\right)-\nabla^{2}\mathbf{A}$}

\begin{eqnarray*}
\nabla\times\left(\nabla\times\mathbf{A}\right) & = & \epsilon_{ijk}\partial_{j}\left(\epsilon_{klm}\partial_{l}A_{m}\right)\mathbf{e}_{i}\\
 & = & \epsilon_{ijk}\epsilon_{klm}\partial_{j}\partial_{l}A_{m}\mathbf{e}_{i}\\
 & = & \epsilon_{kij}\epsilon_{klm}\partial_{j}\partial_{l}A_{m}\mathbf{e}_{i}\\
 & = & \left(\delta_{il}\delta_{jm}-\delta_{im}\delta_{jl}\right)\partial_{j}\partial_{l}A_{m}\mathbf{e}_{i}\\
 & = & \partial_{i}\partial_{j}A_{j}\mathbf{e}_{i}-\partial_{j}\partial_{j}A_{i}\mathbf{e}_{i}\\
 & = & \nabla\left(\nabla\cdot\mathbf{A}\right)-\nabla^{2}\mathbf{A}
\end{eqnarray*}
dove abbiamo usato il fatto che $\epsilon_{ijk}$ non cambia per permutazione ciclica degli indici e l'identità \eqref{eq:EijkEilm}. La relazione sopra si poteva ricavare, con gli opportuni accorgimenti, dalla più generale ($\mathbf{A},\,\mathbf{B},\,\mathbf{C}$ sono tre qualsiasi quantità vettoriali)

\subsubsection*{$\mathbf{A}\times\left(\mathbf{B}\times\mathbf{C}\right)=\mathbf{B}\left(\mathbf{A}\cdot\mathbf{C}\right)-\mathbf{C}\left(\mathbf{A}\cdot\mathbf{B}\right)$}

\begin{eqnarray*}
\mathbf{A}\times\left(\mathbf{B}\times\mathbf{C}\right) & = & \epsilon_{ijk}A_{j}\left(\mathbf{B}\times\mathbf{C}\right)_{k}\mathbf{e}_{i}\\
 & = & \epsilon_{ijk}A_{j}\epsilon_{klm}B_{l}C_{m}\mathbf{e}_{i}\\
 & = & \left(\delta_{il}\delta_{jm}-\delta_{im}\delta_{jl}\right)A_{j}B_{l}C_{m}\mathbf{e}_{i}\\
 & = & B_{i}A_{j}C_{j}\mathbf{e}_{i}-C_{i}A_{j}B_{j}\mathbf{e}_{i}\\
 & = & \mathbf{B}\left(\mathbf{A}\cdot\mathbf{C}\right)-\mathbf{C}\left(\mathbf{A}\cdot\mathbf{B}\right)
\end{eqnarray*}
Vogliamo adesso dare un cenno sui tensori. Solitamente si ha a che fare con due tipi di tensori:
\begin{itemize}
\item Simmetrici $S_{\mu\nu}=a_{\mu}b_{\nu}+a_{\nu}b_{\mu}$, tali che $S_{\mu\nu}=S_{\nu\mu}$
\item Antisimmetrici $A_{\mu\nu}=a_{\mu}b_{\nu}-a_{\nu}b_{\mu}$, tali che $A_{\mu\nu}=-A_{\nu\mu}$
\end{itemize}
Dunque un tensore è qualcosa che trasforma come il prodotto di due vettori: così, per sapere le sue regole di trasformazione basta trasformare i vettori e poi moltiplicarli per dare il tensore. 

In più ogniqualvolta si moltiplicano due tensori con gli stessi indici si ottiene un invariante
\[
S_{\mu\nu}S_{\mu\nu}=invariante
\]
in gergo si ottiene un invariante quando si \emph{contraggono} tutti gli indici di un tensore, cioè la cosa che abbiamo fatto sopra, che ricordiamo, col rischio di essere pedanti, vuol dire
\[
S_{\mu\nu}S_{\mu\nu}\qquad\Leftrightarrow\qquad\sum_{\mu}\sum_{\nu}S_{\mu\nu}S_{\mu\nu}
\]
In particolare, visto che nel caso in cui siamo possiamo rappresentare un tensore come una matrice, per contrarre gli indici basterà fare il prodotto delle due matrici e poi prenderne la traccia.